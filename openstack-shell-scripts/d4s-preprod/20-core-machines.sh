#!/usr/bin/env sh

. ./variables.sh

#
# VM shell
#
openstack --os-cloud d4s-pre server create \
  --image Ubuntu-Jammy-22.04 --flavor m2.small --description "SSH Proxy Jump" \
  --key-name adellam-ed25519 \
  --network d4s-pre-cloud-main --hostname pre-shell \
  --user-data $HOME/Projects/infrascience/cloud-vms-data/cloud-init-openstack-ubuntu.sh \
  --boot-from-volume 100 \
  --security-group default --security-group "Limited SSH access" \
  pre-shell
# Security group that involves the shell server
openstack --os-cloud d4s-pre security group \
  rule create --description "HTTPS from the jump proxy" \
  --ingress --protocol tcp --dst-port 443 --remote-ip <private IP of pre-shell> \
  default
openstack --os-cloud d4s-pre security group \
  rule create --description "HTTP from the jump proxy" \
  --ingress --protocol tcp --dst-port 80 --remote-ip <private IP of pre-shell> \
  default
openstack --os-cloud d4s-pre security group \
  rule create --description "Tomcat debugging on port 8100 from the jump proxy" \
  --ingress --protocol tcp --dst-port 8100 --remote-ip <private IP of pre-shell> \
  default

# VM internal CA
openstack --os-cloud d4s-pre server create \
  --image Ubuntu-Jammy-22.04 --flavor m1.small --description "Internal CA" \
  --key-name adellam-ed25519 --network d4s-pre-cloud-main --hostname internal-ca \
  --user-data $HOME/Projects/infrascience/cloud-vms-data/cloud-init-openstack-ubuntu.sh \
  --boot-from-volume 15 --security-group default \
  internal-ca
# VM prometheus
prometheus m1.large
# Server group anti affinity per HAPROXY
# 2 VM haproxy
haproxy m1.medium
