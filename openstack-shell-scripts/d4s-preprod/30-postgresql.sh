#!/usr/bin/env sh

. ./variables.sh

#
# PostgreSQL
#
# Network for the DB traffic
openstack --os-cloud ${os_infra} network create --no-share --mtu 8942 postgresql-srv-net
openstack --os-cloud ${os_infra} subnet create --network postgresql-srv-net --dhcp --gateway none --subnet-range 192.168.2.0/23 --allocation-pool start=192.168.2.5,end=192.168.3.150 --dns-publish-fixed-ip postgresql-srv-subnet

# Security group that allows postgresql traffic on the dedicated subnet only
openstack --os-cloud ${os_infra} security group create \
  --description "PostgreSQL internal traffic" \
  "PostgreSQL service"
rules_to_delete=$(openstack --os-cloud ${os_infra} security group show -c rules "PostgreSQL service" | grep egress | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud ${os_infra} security group rule delete $r
  done
fi
openstack --os-cloud ${os_infra} security group rule create \
  --description "TCP traffic" \
  --ingress --protocol tcp --dst-port 5432 \
  --remote-ip 192.168.2.0/24 "PostgreSQL service"
#
# PostgreSQL VM
openstack --os-cloud ${os_infra} server create \
  --image Ubuntu-Jammy-22.04 --flavor m1.large \
  --key-name adellam-ed25519 \
  --network ${os_infra}-cloud-main \
  --network postgresql-srv-net \
  --user-data $HOME/Projects/infrascience/cloud-vms-data/cloud-init-openstack-ubuntu.sh \
  --boot-from-volume 10 \
  --min 1 --max 1 \
  --security-group default --security-group "PostgreSQL service" \
  postgresql-server
#
# Data volume for PostgreSQL
openstack --os-cloud ${os_infra} volume create --size 200 --description "PostgreSQL server data disk" postgresql-server-data
openstack --os-cloud ${os_infra} server add volume postgresql-server postgresql-server-data --device /dev/vdb
