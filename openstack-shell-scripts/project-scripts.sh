#!/bin/bash

# S2I2S
openstack --os-cloud ISTI-Cloud zone create --sudo-project-id s2i2s-cloud --email postmaster@isti.cnr.it s2i2s.cloud.isti.cnr.it.
openstack --os-cloud ISTI-Cloud network create --project s2i2s-cloud --no-share --mtu 8942 --dns-domain s2i2s.cloud.isti.cnr.it. --provider-network-type vlan --provider-physical-network datacentre --provider-segment 1001 s2i2s-cloud-main
# *** Network, created by the project manager
openstack --os-cloud s2i2s subnet create --network s2i2s-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99 --subnet-range 10.100.100.0/22 --gateway 10.100.100.1 --dns-publish-fixed-ip s2i2s-cloud-sub
openstack --os-cloud s2i2s router create --description "S2I2S net main router" --external-gateway external-network s2i2s-cloud-external-router
openstack --os-cloud s2i2s router add subnet s2i2s-cloud-external-router s2i2s-cloud-sub

# D4Science dev
openstack --os-cloud d4s-dev zone create --email postmaster@isti.cnr.it cloud-dev.d4science.org.
openstack --os-cloud d4s-dev network create --no-share --mtu 8942 --dns-domain cloud-dev.d4science.org. d4s-dev-cloud-main
openstack --os-cloud d4s-dev subnet create --network d4s-dev-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99  --subnet-range 10.1.28.0/22 --gateway 10.1.28.1 --dns-publish-fixed-ip d4s-dev-cloud-sub
openstack --os-cloud d4s-dev router create --description "D4S Dev main router" --external-gateway external-network d4s-dev-cloud-external-router
openstack --os-cloud d4s-dev router add subnet d4s-dev-cloud-external-router d4s-dev-cloud-sub

# D4Science production
openstack --os-cloud d4s-production zone create --email postmaster@isti.cnr.it cloud.d4science.org.
openstack --os-cloud d4s-production network create --description "D4S Production main private network" --no-share --mtu 8942 --dns-domain cloud.d4science.org. d4s-production-cloud-main
openstack --os-cloud d4s-production subnet create  --description "D4S Production main subnet" --network d4s-production-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99  --subnet-range 10.1.36.0/22 --gateway 10.1.36.1 --dns-publish-fixed-ip d4s-production-cloud-sub
openstack --os-cloud d4s-production router create --description "D4S Production main router" --external-gateway external-network d4s-production-cloud-external-router
openstack --os-cloud d4s-production router add subnet d4s-production-cloud-external-router d4s-production-cloud-sub

# D-Net
openstack --os-cloud d-net zone create --email postmaster@isti.cnr.it openaire.d4science.org.
openstack --os-cloud d-net network create --description "D-Net main private network" --no-share --mtu 8942 --dns-domain openaire.d4science.org. d-net-cloud-main
openstack --os-cloud d-net subnet create  --description "D-Net main subnet" --network d-net-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99  --subnet-range 10.1.40.0/22 --gateway 10.1.40.1 --dns-publish-fixed-ip d-net-cloud-sub
openstack --os-cloud d-net router create --description "D-Net main router" --external-gateway external-network d-net-cloud-external-router
openstack --os-cloud d-net router add subnet d-net-cloud-external-router d-net-cloud-sub



#
#
# PRE
#
#
# D4Science pre
# Networking
#
openstack --os-cloud d4s-pre zone create --email postmaster@isti.cnr.it cloud-pre.d4science.org.
openstack --os-cloud d4s-pre network create --description "D4S Pre main private network" --no-share --mtu 8942 --dns-domain cloud-pre.d4science.org. d4s-pre-cloud-main
openstack --os-cloud d4s-pre subnet create  --description "D4S Pre main subnet" --network d4s-pre-cloud-main --dhcp --dns-nameserver 146.48.29.97 --dns-nameserver 146.48.29.98 --dns-nameserver 146.48.29.99  --subnet-range 10.1.32.0/22 --gateway 10.1.32.1 --dns-publish-fixed-ip d4s-pre-cloud-sub
openstack --os-cloud d4s-pre router create --description "D4S Pre main router" --external-gateway external-network d4s-pre-cloud-external-router
openstack --os-cloud d4s-pre router add subnet d4s-pre-cloud-external-router d4s-pre-cloud-sub
#
# Part of the security groups
#
# Remove the ingress rules from the default group
# openstack --os-cloud d4s-pre security group show default
rules_to_delete=
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules default | grep ingress | grep -v protocol | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi
openstack --os-cloud d4s-pre security group rule create \
  --description "Allow ICMP" --ingress --protocol icmp \
  --remote-ip 0.0.0.0/0 default
openstack --os-cloud d4s-pre security group rule create \
  --description "Prometheus node exporter" --ingress --protocol tcp \
  --dst-port "9100" \
  --remote-ip 10.1.32.0/22 default

# SSH access
openstack --os-cloud d4s-pre security group create \
  --description "Access to the SSH Proxy Jump server" \
  "Limited SSH access"
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules "Limited SSH access" | grep egress | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from S2I2S vpn 1" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.28.10/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from S2I2S vpn 2" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.28.11/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from D4Science VPN 1" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.27/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from D4Science VPN 2" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.49/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "Access from shell.d4science.org" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.95/32 "Limited SSH access"
openstack --os-cloud d4s-pre security group rule create \
  --description "SSH from the InfraScience Network (because masquerade on the Xen hypervisors)" --ingress --protocol tcp --dst-port "22" \
  --remote-ip 146.48.122.0/23 "Limited SSH access"
# Limited HTTPS access
openstack --os-cloud d4s-pre security group create \
  --description "Limited HTTPs and public HTTP. For servers with public IP addresses that can be accessed from our VPN only" \
  "Limited HTTPS access"
rules_to_delete=$(openstack --os-cloud d4s-pre security group show -c rules "Limited HTTPS access" | grep egress | awk -F id= '{ print $2 }' | awk -F \' '{ print $2 }')
if [ -n "$rules_to_delete" ] ; then
  for r in $(echo $rules_to_delete) ; do
    openstack --os-cloud d4s-pre security group rule delete $r
  done
fi

#
# VM shell
#
openstack --os-cloud d4s-pre server create \
  --image Ubuntu-Jammy-22.04 --flavor m2.small --description "SSH Proxy Jump" \
  --key-name adellam-ed25519 \
  --network d4s-pre-cloud-main --hostname pre-shell \
  --user-data $HOME/Projects/infrascience/cloud-vms-data/cloud-init-openstack-ubuntu.sh \
  --boot-from-volume 100 \
  --security-group default --security-group "Limited SSH access" \
  pre-shell
# Security group that involves the shell server
openstack --os-cloud d4s-pre security group \
  rule create --description "HTTPS from the jump proxy" \
  --ingress --protocol tcp --dst-port 443 --remote-ip <private IP of pre-shell> \
  default
openstack --os-cloud d4s-pre security group \
  rule create --description "HTTP from the jump proxy" \
  --ingress --protocol tcp --dst-port 80 --remote-ip <private IP of pre-shell> \
  default
openstack --os-cloud d4s-pre security group \
  rule create --description "Tomcat debugging on port 8100 from the jump proxy" \
  --ingress --protocol tcp --dst-port 8100 --remote-ip <private IP of pre-shell> \
  default

# VM internal CA
openstack --os-cloud d4s-pre server create \
  --image Ubuntu-Jammy-22.04 --flavor m1.small --description "Internal CA" \
  --key-name adellam-ed25519 --network d4s-pre-cloud-main --hostname internal-ca \
  --user-data $HOME/Projects/infrascience/cloud-vms-data/cloud-init-openstack-ubuntu.sh \
  --boot-from-volume 15 --security-group default \
  internal-ca
# VM prometheus
prometheus m1.large
# Server group anti affinity per HAPROXY
# 2 VM haproxy
haproxy m1.medium
# Octavia service
# Security group per haproxy (servono gli IP privati di octavia)
#
