#!/bin/bash

yum -y install python policycoreutils-python

/sbin/useradd --system --home-dir /srv/ansible -m --shell /bin/bash -c "Used for the Ansible provisioning tasks" ansible

# SSH keys of users authorized to execute ansible playbooks.
# The ones in the example belong to Andrea Dell'Amico and Tommaso Piccioli.
# Feel free to add yours if you are entitled to run the ansible provisioning on that server

mkdir /srv/ansible/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzcHuDU7PgJwz34AsVG0E2+ZRx17ZKW1uDEGABNk3Z60/c9LTwWKPj6kcIRy6RzFJI5X+IgPJnYouXVmJsIWjVL8IRk8fP1ffJC6Fyf6H7+fCxu/Wwed5OoOCvKeZ0bEmJ1tlXFM6+EnxKqLCvz3fsNy8e4WKMnpS1hT8K6YB7PMjt60S3wOaxds1Lv4NmmgnfGM5uZFYrZCx1/GJCzNSh7AEEEUIVQ1B8xmXbet7whNiwDmiOnXSlt38dkIYT8kNMuRCj/r9wPr7FmoUCOFzUVXTcnuYagKyURrZ8QDyHbK6XQLYXgvCz/lWoErGFbDqpmBHHyvKSeLPxYfJpWJ70w== tom@tom" > /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ9n6B+J5S7NPnwjejPC2WrvcRzC07WPnAoQ7ZHZ0Mv9JakyWItswzI3Drz/zI0mCamyuye+9dWz9v/ZRwUfBobVyXuptRaZIwxlMC/KsTZofpp3RHOBTteZ4/VM0VhEeiOHu+GuzNE0fRB2gsusWeMMae2cq4TjVAOMcQmJX496L703Smc14gFrP8y/P9jbC5HquuVnPR29PsW4mHidPmjdKkO7QmDfFAj44pEUGeInYOJe708C03NCpsjHw8AVdAJ6Pf16EOdDH+z8D6CByVO3s8UT0HJ85BRoIy6254/hmYLzyd/eRnCXHS/dke+ivrlA3XxG4+DmqjuJR/Jpfx adellam@semovente" >> /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAjmoNSXtyx1TKOr8eJDfO1mRdX0JxQmf5N9OnMrqtF3DpJCWslws5NIDQBEsERPhdHlljUM9Y6lXpt5CWiBHH+DiSo1tE4FQ74l41tkwVqW9tGrFinWEaGRfb84KYJc0vmW5lPdGGKY+KKlhKDF1TFcTGQfkh6ZD4BXU1Uc11nR11XD3OAAcltNfuw9biN1kUQjMCRztytJEOW1nfLtEufaZMGoE8+RMS8PVWlPcjTe/ACkEixvimwGEiyZEi2uiWIqocja/wyKCJikgrWB6Zg24bouM564V4n+0JUomNgVSjFT8TXv5hecNt8k3HRMGMyLaeaGiafbsNgicG3s3beQ== g.panichi@isti.cnr.it" >> /srv/ansible/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCmPtZk27hz8oJmwCLookQLtTr7gfiBZtHm6nkoZPagrlBkoP1JTFnEb6EP89RC4fYTjkcDi/+B18Fv3lHCrqGJeoGF/hhXpC1t+/lI2JSYu3XunxDXVfyygAorPfwQmwDC/CM7CkEW/xKiM3yXtiBW/y6Qj387IMFQTZmPp/nLKr6Pe20hOTsVTPwehKK45ID5iIpEauPgvMzWLgFTYBPLY3/ypTgbTSbyCs1ZmyVN8ajkP5c7Xg1GKdTYNLhDztgzqE3Gzpo2y6piTybFvYHMgA7hZmrlhmzUi7DFFVEm0FK7EWXHXDFXOarbDdA2q7L8SAib40zRRXByZWAghp6rqoI89NokH36FISK+/eL11kXGJM4vDrpE8zrM/FxE5vPNX0zeQw0Q4kiSnL5pN5XttJvP94r6uZzp6G8hudJFXZjxJghKflQ+xbZaBTLYGoX6OcuzYqO9jGbUFTfc/rADi7gHWVzC/rc7KDHnJTy6HPoicWT9iElmkZ2GSmDd82LUvcm6XPspfDf+GiKPYdiFgJ5MhA55gosIon7XbVv3uONOptWGpGWZOXcxgahz1QkX4eB2WdlRPd+zMJ0gsYDqtq23PG6EOXWe4alIV4CKgSGt97I7Fa9+FXJ2QzMM7sVowPJeIkvrCJIBIF63OHlMwgDw826+P17ZIOiEBbNB3Q== marco.procaccini@cnr.it" >> /srv/ansible/.ssh/authorized_keys
cat ~ubuntu/.ssh/authorized_keys >> /srv/ansible/.ssh/authorized_key
/bin/chown -R ansible:ansible /srv/ansible
/bin/chmod 700 /srv/ansible/.ssh
mkdir -p /etc/sudoers.d
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible-user
/bin/chmod 600 /etc/sudoers.d/ansible-user

semanage fcontext -a -e /home /srv/ansible ; restorecon -vR /srv/ansible

