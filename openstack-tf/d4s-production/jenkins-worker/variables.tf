#variables

variable "jenkins_env" {
  type = map(string)
  default = {
    name              = "jenkins-worker"
    flavor            = "m1.xlarge"
    vol_data_name     = "jenkins-data"
    vol_data_size     = "100"
    vol_data_device   = "/dev/vdb"
    vm_count          = "2"
  }
}
