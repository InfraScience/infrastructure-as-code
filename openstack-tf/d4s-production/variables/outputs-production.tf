output "os_project_data" {
  value = var.os_project_data
}

output "dns_zone" {
  value = var.dns_zone
}

output "default_security_group_name" {
  value = "default_for_all"
}

output "main_private_network" {
  value = var.main_private_network
}

output "main_private_subnet" {
  value = var.main_private_subnet
}

output "external_router" {
  value = var.external_router
}

output "basic_services_ip" {
  value = var.basic_services_ip
}

output "main_haproxy_l7_ip" {
  value = var.main_haproxy_l7_ip
}

output "octavia_information" {
  value = var.octavia_information
}

output "access_postgresql_security_group" {
  value = openstack_networking_secgroup_v2.shared_postgresql_access
}

