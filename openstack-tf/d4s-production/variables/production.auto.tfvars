#
os_project_data = {
    id = "1b45adf388934758b56d0dfdb4bfacf3"
}

dns_zone = {
    zone_name   = "cloud.d4science.org."
    email       = "postmaster@isti.cnr.it"
    description = "DNS primary zone for the d4s-production-cloud project"
    ttl         = 8600
}

default_security_group_name = "default_for_all"

main_private_network = {
    name        = "d4s-production-cloud-main"
    description = "D4Science Production private network (use this as the main network)"
}

main_private_subnet = {
    name             = "d4s-production-cloud-main-subnet"
    description      = "D4Science Production main private subnet"
    cidr             = "10.1.40.0/21"
    gateway_ip       = "10.1.40.1"
    allocation_start = "10.1.41.100"
    allocation_end   = "10.1.47.254"
}

external_router = {
    name        = "d4s-production-cloud-external-router"
    description = "D4Science Production main router"
    id          = "cc26064a-bb08-4c0b-929f-d0cb39f934a3"
}

basic_services_ip = {
    ca                = "10.1.40.4"
    ca_cidr           = "10.1.40.4/32"
    ssh_jump          = "10.1.40.5"
    ssh_jump_cidr     = "10.1.40.5/32"
    prometheus        = "10.1.40.10"
    prometheus_cidr   = "10.1.40.10/32"
    haproxy_l7_1      = "10.1.40.11"
    haproxy_l7_1_cidr = "10.1.40.11/32"
    haproxy_l7_2      = "10.1.40.12"
    haproxy_l7_2_cidr = "10.1.40.12/32"
    octavia_main      = "10.1.40.20"
    octavia_main_cidr = "10.1.40.20/32"
}

main_haproxy_l7_ip = ["10.1.40.11", "10.1.40.12"]

octavia_information = {
    main_lb_name        = "d4s-production-cloud-l4-load-balancer"
    main_lb_description = "Main L4 load balancer for the D4Science production"
    swarm_lb_name       = "d4s-production-cloud-l4-swarm-load-balancer"
    octavia_flavor      = "octavia_amphora-mvcpu-ha"
    octavia_flavor_id   = "394988b5-6603-4a1e-a939-8e177c6681c7"
    main_lb_hostname    = "main-lb"
}

