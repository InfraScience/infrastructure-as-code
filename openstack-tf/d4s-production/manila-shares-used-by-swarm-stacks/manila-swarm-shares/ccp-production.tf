# NFS shares required by the CCP
# Create a NFS share for the repository data
#
resource "openstack_sharedfilesystem_share_v2" "ccp_production_repository_data" {
  name        = "ccp_production_repository_data"
  description = "NFS share for the CCP repository data"
  share_proto = "NFS"
  size        = 5
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "ccp_production_repository_access_swarm_mgr" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.ccp_production_repository_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "ccp_production_repository_access_swarm_workers" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.ccp_production_repository_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

# NFS shares required by the CCP
# Create a NFS share for the method logs
#
resource "openstack_sharedfilesystem_share_v2" "ccp_production_methods_logs" {
  name        = "ccp_production_method_logs"
  description = "NFS share for the CCP method logs"
  share_proto = "NFS"
  size        = 2
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "ccp_production_methods_logs_access_swarm_mgr" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.ccp_production_methods_logs.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "ccp_production_methods_logs_access_swarm_workers" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.ccp_production_methods_logs.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}


output "ccp_production_repository_data" {
  value = openstack_sharedfilesystem_share_v2.ccp_production_repository_data
}

output "ccp_production_repository_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.ccp_production_repository_access_swarm_mgr
  sensitive = true
}

output "ccp_production_repository_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.ccp_production_repository_access_swarm_workers
  sensitive = true
}

output "ccp_production_methods_logs" {
  value = openstack_sharedfilesystem_share_v2.ccp_production_methods_logs
}

output "ccp_production_methods_logs_access_swarm_mgr" {
  value     = openstack_sharedfilesystem_share_access_v2.ccp_production_methods_logs_access_swarm_mgr
  sensitive = true
}

output "ccp_production_methods_logs_access_swarm_workers" {
  value     = openstack_sharedfilesystem_share_access_v2.ccp_production_methods_logs_access_swarm_workers
  sensitive = true
}
