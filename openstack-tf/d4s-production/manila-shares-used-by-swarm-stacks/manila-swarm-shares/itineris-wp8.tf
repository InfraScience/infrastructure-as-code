# itineris_wp8_pgadmin_data
resource "openstack_sharedfilesystem_share_v2" "itineris_wp8_pgadmin" {
  name        = "itineris_wp8_pgadmin"
  description = "NFS share for the ITINERIS WP8 pgAdmin instance"
  share_proto = "NFS"
  size        = 6
}

output "itineris_wp8_pgadmin" {
  value = openstack_sharedfilesystem_share_v2.itineris_wp8_pgadmin
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "itineris_wp8_pgadmin_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.itineris_wp8_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "itineris_wp8_pgadmin_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.itineris_wp8_pgadmin_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "itineris_wp8_pgadmin_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.itineris_wp8_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "itineris_wp8_pgadmin_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.itineris_wp8_pgadmin_swarm_workers_acl
  sensitive = true
}

# Temporary data for the itineris isotope webapp
resource "openstack_sharedfilesystem_share_v2" "itineris_wp8_data" {
  name        = "itineris_wp8_data"
  description = "NFS share for the ITINERIS WP8 local data"
  share_proto = "NFS"
  size        = 5
}

output "itineris_wp8_data" {
  value = openstack_sharedfilesystem_share_v2.itineris_wp8_data
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "itineris_wp8_data_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.itineris_wp8_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "itineris_wp8_data_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.itineris_wp8_data_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "itineris_wp8_data_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.itineris_wp8_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "itineris_wp8_data_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.itineris_wp8_data_swarm_workers_acl
  sensitive = true
}
