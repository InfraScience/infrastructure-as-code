# open_asfa_test_couchbase
resource "openstack_sharedfilesystem_share_v2" "open_asfa_test_couchbase" {
  name        = "open_asfa_test_couchbase"
  description = "NFS share for the TEST OpenASFA couchbase instance"
  share_proto = "NFS"
  size        = 5
}

output "open_asfa_test_couchbase" {
  value = openstack_sharedfilesystem_share_v2.open_asfa_test_couchbase
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_couchbase_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_couchbase.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_couchbase_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_couchbase_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_couchbase_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_couchbase.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_couchbase_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_couchbase_swarm_workers_acl
  sensitive = true
}

# open_asfa_test_postgresql
resource "openstack_sharedfilesystem_share_v2" "open_asfa_test_postgresql" {
  name        = "open_asfa_test_postgresql"
  description = "NFS share for the TEST OpenASFA postgresql instance"
  share_proto = "NFS"
  size        = 3
}

output "open_asfa_test_postgresql" {
  value = openstack_sharedfilesystem_share_v2.open_asfa_test_postgresql
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_postgresql_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_postgresql.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_postgresql_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_postgresql_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_postgresql_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_postgresql.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_postgresql_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_postgresql_swarm_workers_acl
  sensitive = true
}

# open_asfa_test_pgadmin_data
resource "openstack_sharedfilesystem_share_v2" "open_asfa_test_pgadmin" {
  name        = "open_asfa_test_pgadmin"
  description = "NFS share for the TEST OpenASFA pgAdmin instance"
  share_proto = "NFS"
  size        = 6
}

output "open_asfa_test_pgadmin" {
  value = openstack_sharedfilesystem_share_v2.open_asfa_test_pgadmin
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_pgadmin_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_pgadmin_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_pgadmin_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "open_asfa_test_pgadmin_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.open_asfa_test_pgadmin.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "open_asfa_test_pgadmin_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.open_asfa_test_pgadmin_swarm_workers_acl
  sensitive = true
}
