# NFS shares required by 
# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "webodv_data" {
  name        = "fairease_webodv_data"
  description = "NFS share for the FAIR-EASE webodv data volume"
  share_proto = "NFS"
  size        = 10
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "webodv_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "webodv_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "webodv_ODV" {
  name        = "fairease_webodv_ODV"
  description = "NFS share for the FAIR-EASE webodv ODV volume"
  share_proto = "NFS"
  size        = 10
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "webodv_ODV_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_ODV.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "webodv_ODV_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.webodv_ODV.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}



output "webodv_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.webodv_data
}

output "webodv_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.webodv_data_mgr_share_access
  sensitive = true
}

output "webodv_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.webodv_data_workers_share_access
  sensitive = true
}

output "webodv_nfs_ODV" {
  value = openstack_sharedfilesystem_share_v2.webodv_ODV
}

output "webodv_nfs_ODV_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.webodv_ODV_mgr_share_access
  sensitive = true
}

output "webodv_nfs_ODV_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.webodv_ODV_workers_share_access
  sensitive = true
}

