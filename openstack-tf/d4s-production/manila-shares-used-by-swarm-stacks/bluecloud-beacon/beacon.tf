# NFS shares required by Bluecloud beacon service

# Create a NFS share for emodnet_chemistry

resource "openstack_sharedfilesystem_share_v2" "beacon_emodnet_chemistry" {
  name        = "beacon_emodnet_chemistry"
  description = "NFS share for the Bluecloud beacon_emodnet_chemistry data volume"
  share_proto = "NFS"
  size        = 20
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_emodnet_chemistry_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_emodnet_chemistry.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_emodnet_chemistry_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_emodnet_chemistry.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_emodnet_chemistry_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_emodnet_chemistry
}

output "beacon_emodnet_chemistry_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_emodnet_chemistry_mgr_share_access
  sensitive = true
}

output "beacon_emodnet_chemistry_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_emodnet_chemistry_workers_share_access
  sensitive = true
}




# Create a NFS share for wod
resource "openstack_sharedfilesystem_share_v2" "beacon_wod_data" {
  name        = "beacon_wod_data"
  description = "NFS share for the Bluecloud beacon_wod_data data volume"
  share_proto = "NFS"
  size        = 700
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_wod_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_wod_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_wod_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_wod_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_wod_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_wod_data
}

output "beacon_wod_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_wod_data_mgr_share_access
  sensitive = true
}

output "beacon_wod_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_wod_data_workers_share_access
  sensitive = true
}



# Create a NFS share for BGC
resource "openstack_sharedfilesystem_share_v2" "beacon_bgc_data" {
  name        = "beacon_bgc_data"
  description = "NFS share for the Bluecloud beacon_bgc_data data volume"
  share_proto = "NFS"
  size        = 370
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_bgc_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_bgc_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_bgc_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_bgc_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_bgc_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_bgc_data
}

output "beacon_bgc_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_bgc_data_mgr_share_access
  sensitive = true
}

output "beacon_bgc_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_bgc_data_workers_share_access
  sensitive = true
}




# Create a NFS share for CORA PR
resource "openstack_sharedfilesystem_share_v2" "beacon_cora_pr_data" {
  name        = "beacon_cora_pr_data"
  description = "NFS share for the Bluecloud beacon_cora_pr_data data volume"
  share_proto = "NFS"
  size        = 440
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_cora_pr_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_cora_pr_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_cora_pr_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_cora_pr_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_cora_pr_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_cora_pr_data
}

output "beacon_cora_pr_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_cora_pr_data_mgr_share_access
  sensitive = true
}

output "beacon_cora_pr_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_cora_pr_data_workers_share_access
  sensitive = true
}



# Create a NFS share for CORA TS
resource "openstack_sharedfilesystem_share_v2" "beacon_cora_ts_data" {
  name        = "beacon_cora_ts_data"
  description = "NFS share for the Bluecloud beacon_cora_ts_data data volume"
  share_proto = "NFS"
  size        = 70
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_cora_ts_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_cora_ts_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_cora_ts_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_cora_ts_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_cora_ts_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_cora_ts_data
}

output "beacon_cora_ts_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_cora_ts_data_mgr_share_access
  sensitive = true
}

output "beacon_cora_ts_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_cora_ts_data_workers_share_access
  sensitive = true
}


# Create a NFS share for argo
resource "openstack_sharedfilesystem_share_v2" "beacon_argo_data" {
  name        = "beacon_argo_data"
  description = "NFS share for the Bluecloud beacon_argo_data data volume"
  share_proto = "NFS"
  size        = 340
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_argo_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_argo_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_argo_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_argo_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_argo_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_argo_data
}

output "beacon_argo_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_argo_data_mgr_share_access
  sensitive = true
}

output "beacon_argo_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_argo_data_workers_share_access
  sensitive = true
}


# Create a NFS share for seadatanet_ts
resource "openstack_sharedfilesystem_share_v2" "beacon_seadatanet_ts_data" {
  name        = "beacon_seadatanet_ts_data"
  description = "NFS share for the Bluecloud beacon_seadatanet_ts_data data volume"
  share_proto = "NFS"
  size        = 200
}

# Allow access to the NFS share to Swarm Managers and Workers
resource "openstack_sharedfilesystem_share_access_v2" "beacon_seadatanet_ts_data_mgr_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_seadatanet_ts_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "beacon_seadatanet_ts_data_workers_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.beacon_seadatanet_ts_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "beacon_seadatanet_ts_data_nfs_data" {
  value = openstack_sharedfilesystem_share_v2.beacon_seadatanet_ts_data
}

output "beacon_seadatanet_ts_data_nfs_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_seadatanet_ts_data_mgr_share_access
  sensitive = true
}

output "beacon_seadatanet_ts_data_nfs_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.beacon_seadatanet_ts_data_workers_share_access
  sensitive = true
}
