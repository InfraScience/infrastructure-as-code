# Blue Cloud Clone Wars, see https://support.d4science.org/issues/28608
# mount point: /runtime/data
resource "openstack_sharedfilesystem_share_v2" "clone_wars_data" {
  name        = "clone_wars_data"
  description = "NFS share for the Clone Wars data"
  share_proto = "NFS"
  size        = 1000
}

output "clone_wars_data" {
  value = openstack_sharedfilesystem_share_v2.clone_wars_data
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "clone_wars_data_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.clone_wars_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "clone_wars_data_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.clone_wars_data_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "clone_wars_data_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.clone_wars_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "clone_wars_data_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.clone_wars_data_swarm_workers_acl
  sensitive = true
}

# Logs
# Mount point: /runtime/log
resource "openstack_sharedfilesystem_share_v2" "clone_wars_logs" {
  name        = "clone_wars_logs"
  description = "NFS share for the Clone Wars logs"
  share_proto = "NFS"
  size        = 50
}

output "clone_wars_logs" {
  value = openstack_sharedfilesystem_share_v2.clone_wars_logs
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "clone_wars_logs_swarm_mgr_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.clone_wars_logs.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "clone_wars_logs_swarm_mgr_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.clone_wars_logs_swarm_mgr_acl
  sensitive = true
}

resource "openstack_sharedfilesystem_share_access_v2" "clone_wars_logs_swarm_workers_acl" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.clone_wars_logs.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "clone_wars_logs_swarm_workers_acl" {
  value     = openstack_sharedfilesystem_share_access_v2.clone_wars_logs_swarm_workers_acl
  sensitive = true
}
