# NFS shares required by 
# Create a NFS share
resource "openstack_sharedfilesystem_share_v2" "intelcomp_elasticsearch_data" {
  name        = "scilake_intelcomp_elasticsearch_data"
  description = "NFS share for the scilake intelcomp elasticsearch data"
  share_proto = "NFS"
  size        = 50
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_mgr_elasticsearch_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_elasticsearch_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_workers_elasticsearch_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_elasticsearch_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "intelcomp_activemq_data" {
  name        = "scilake_intelcomp_activemq_data"
  description = "NFS share for the scilake intelcomp activemq data"
  share_proto = "NFS"
  size        = 5
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_mgr_activemq_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_activemq_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_workers_activemq_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_activemq_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "intelcomp_redis_cache" {
  name        = "scilake_intelcomp_redis_cache"
  description = "NFS share for the scilake intelcomp redis cache"
  share_proto = "NFS"
  size        = 2
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_mgr_redis_cache_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_redis_cache.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_workers_redis_cache_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_redis_cache.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_v2" "intelcomp_pgadmin_data" {
  name        = "scilake_intelcomp_pgadmin_data"
  description = "NFS share for the scilake intelcomp pgadmin data"
  share_proto = "NFS"
  size        = 10
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_mgr_pgadmin_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_managers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_pgadmin_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

resource "openstack_sharedfilesystem_share_access_v2" "intelcomp_workers_pgadmin_data_share_access" {
  for_each     = { for nfs_ip in data.terraform_remote_state.main_infrastructure.outputs.swarm_workers_nfs_ip_ports : join("", nfs_ip.all_fixed_ips) => nfs_ip }
  share_id     = openstack_sharedfilesystem_share_v2.intelcomp_pgadmin_data.id
  access_type  = "ip"
  access_to    = each.key
  access_level = "rw"
}

output "intelcomp_nfs_elasticsearch_data" {
  value = openstack_sharedfilesystem_share_v2.intelcomp_elasticsearch_data
}

output "intelcomp_nfs_elasticsearch_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_mgr_elasticsearch_data_share_access
  sensitive = true
}

output "intelcomp_nfs_elasticsearch_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_workers_elasticsearch_data_share_access
  sensitive = true
}

output "intelcomp_nfs_activemq_data" {
  value = openstack_sharedfilesystem_share_v2.intelcomp_activemq_data
}

output "intelcomp_nfs_activemq_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_mgr_activemq_data_share_access
  sensitive = true
}

output "intelcomp_nfs_activemq_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_workers_activemq_data_share_access
  sensitive = true
}

output "intelcomp_nfs_redis_cache" {
  value = openstack_sharedfilesystem_share_v2.intelcomp_redis_cache
}

output "intelcomp_nfs_redis_cache_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_mgr_redis_cache_share_access
  sensitive = true
}

output "intelcomp_nfs_redis_cache_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_workers_redis_cache_share_access
  sensitive = true
}

output "intelcomp_nfs_pgadmin_data" {
  value = openstack_sharedfilesystem_share_v2.intelcomp_pgadmin_data
}

output "intelcomp_nfs_pgadminl_data_mgr_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_mgr_pgadmin_data_share_access
  sensitive = true
}

output "intelcomp_nfs_pgadmin_data_workers_acls" {
  value     = openstack_sharedfilesystem_share_access_v2.intelcomp_workers_pgadmin_data_share_access
  sensitive = true
}

