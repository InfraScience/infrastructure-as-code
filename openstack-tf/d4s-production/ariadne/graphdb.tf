resource "openstack_blockstorage_volume_v3" "graphdb_ariadne_data_volume" {
  name   = "graphdb_ariadne_data_volume"
  size   = "200"
}

resource "openstack_blockstorage_volume_v3" "graphdb_test_ariadne_data_volume" {
  name   = "graphdb_test_ariadne_data_volume"
  size   = "100"
}

# Instances
resource "openstack_compute_instance_v2" "graphdb_ariadne" {
  name   = "graphdb-ariadne"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.d1_large
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  
  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_instance_v2" "graphdb_test_ariadne" {
  name   = "graphdb-test-ariadne"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.d1_large
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  
  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "graphdb_ariadne_attach" {
  instance_id = openstack_compute_instance_v2.graphdb_ariadne.id
  volume_id   = openstack_blockstorage_volume_v3.graphdb_ariadne_data_volume.id
  device = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "graphdb_test_ariadne_attach" {
  instance_id = openstack_compute_instance_v2.graphdb_test_ariadne.id
  volume_id   = openstack_blockstorage_volume_v3.graphdb_test_ariadne_data_volume.id
  device = "/dev/vdb"
}