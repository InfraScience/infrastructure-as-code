mongodb_cluster_data = {
  count           = 4
  name            = "mongodb-replica"
  flavor          = "m2.large"
  data_disk_size  = 10000
  swap_disk_size  = 8
  image_type_name = "Ubuntu-Focal-20.04"
  image_type_uuid = "75c23040-2be7-49e9-8029-a16dc9f755d1"
}

mongodb_ip = ["10.1.40.22", "10.1.40.25", "10.1.40.24", "10.1.40.23"]

mongodb_vol_data = {
  name            = "mongodb-vol"
  flavor          = "m1.medium"
  data_disk_size  = 200
  swap_disk_size  = 8
  image_type_name = "Ubuntu-Focal-20.04"
  image_type_uuid = "75c23040-2be7-49e9-8029-a16dc9f755d1"
}

mongodb_vol_ip = "10.1.40.29"
