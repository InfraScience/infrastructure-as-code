# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#
# Creates the server group "accounting-service"
#
resource "openstack_compute_servergroup_v2" "accounting_service_server_group" {
  name     = "accounting-service"
  policies = [module.common_variables.policy_list.anti_affinity]
}

module "instance_without_data_volume" {
  source                = "../../modules/cassandra"
  cassandra_nodes_count = 3
  cassandra_node_flavor = module.common_variables.flavor_list.m1_medium
  cassandra_server_data = {
    node_name             = "cassandra"
    node_data_disk_size   = 20
    node_data_disk_device = "/dev/vdb"
  }

  cassandra_tcp_ports_map = {
    tcp_plain = {
      description = "Cassandra TCP port 7000",
      port_min    = 7000,
      port_max    = 7000
    },
    tcp_tls = {
      description = "Cassandra TLS TCP port 7001",
      port_min    = 7001,
      port_max    = 7001
    },
    tcp_transport = {
      description = "Cassandra TCP transport 9042",
      port_min    = 9042,
      port_max    = 9042
    },
    rpc = {
      description = "Cassandra TCP transport 9042",
      port_min    = 9160,
      port_max    = 9160
    }
  }
}
