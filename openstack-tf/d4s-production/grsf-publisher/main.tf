# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#
# Creates the server group "grsf-publisher" for context GRSF
#
resource "openstack_compute_servergroup_v2" "grsf_publisher_grsf_server_group" {
  name     = "grsf-publisher-grsf"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "grsf-publisher" for context GRSF_Admin
#
resource "openstack_compute_servergroup_v2" "grsf_publisher_grsf_admin_server_group" {
  name     = "grsf-publisher-grsf-admin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "grsf-publisher" for context GRSF_Pre
#
resource "openstack_compute_servergroup_v2" "grsf_publisher_grsf_pre_server_group" {
  name     = "grsf-publisher-grsf-pre"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}


module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    grsf_publisher = {
      name              = "grsf-publisher-grsf",
      description       = "This instance serves GRSF Publisher service for GRSF VRE",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.debugging_from_jump_node],
      server_groups_ids = [openstack_compute_servergroup_v2.grsf_publisher_grsf_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    grsf_publisher_grsf_admin = {
      name              = "grsf-publisher-grsf-admin",
      description       = "This instance serves GRSF Publisher service for GRSF_Admin VRE",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.debugging_from_jump_node],
      server_groups_ids = [openstack_compute_servergroup_v2.grsf_publisher_grsf_admin_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    grsf_publisher_grsf_pre = {
      name              = "grsf-publisher-grsf-pre",
      description       = "This instance serves GRSF Publisher service for GRSF_Pre VRE",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.debugging_from_jump_node],
      server_groups_ids = [openstack_compute_servergroup_v2.grsf_publisher_grsf_pre_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    }
  }
  
}
