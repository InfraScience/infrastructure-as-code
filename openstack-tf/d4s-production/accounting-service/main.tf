# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#
# Creates the server group "accounting-service"
#
resource "openstack_compute_servergroup_v2" "accounting_service_server_group" {
  name     = "accounting-service"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    accounting_service_1 = {
      name              = "accounting-service-1",
      description       = "This instance serves accounting service",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.timescaledb],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.debugging_from_jump_node],
      server_groups_ids = [openstack_compute_servergroup_v2.accounting_service_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    accounting_service_2 = {
      name              = "accounting-service-2",
      description       = "This instance serves accounting service",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.timescaledb],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.debugging_from_jump_node],
      server_groups_ids = [openstack_compute_servergroup_v2.accounting_service_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    }
  }
}
