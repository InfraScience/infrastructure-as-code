# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

provider "openstack" {
  # cloud = "d4s-production"
  cloud = "ISTI-Cloud"
}

module "common_variables" {
  source = "../../modules/common_variables"
}
# Main module
module "main_private_net_and_dns_zone" {
  source = "../../modules/main_private_net_and_dns_zone"
  dns_zone = {
    zone_name   = "cloud.d4science.org."
    email       = "postmaster@isti.cnr.it"
    description = "DNS primary zone for the d4s-production-cloud project"
    ttl         = 8600
  }
  os_project_data = {
      id = "1b45adf388934758b56d0dfdb4bfacf3"
  }
  main_private_network = {
    name        = "d4s-production-cloud-main"
    description = "D4Science Production private network (use this as the main network)"
  }
  main_private_subnet = {
    name             = "d4s-production-cloud-main-subnet"
    description      = "D4Science Production main private subnet"
    cidr             = "10.1.40.0/21"
    gateway_ip       = "10.1.40.1"
    allocation_start = "10.1.41.100"
    allocation_end   = "10.1.47.254"
  }
  external_router = {
    name        = "d4s-production-cloud-external-router"
    description = "D4Science Production main router"
  }

}

output "dns_zone_id" {
  value = module.main_private_net_and_dns_zone.dns_zone_id
}

output "main_private_network_id" {
  value = module.main_private_net_and_dns_zone.main_private_network_id
}

output "main_subnet_network_id" {
  value = module.main_private_net_and_dns_zone.main_subnet_network_id
}

output "external_gateway_ip" {
  value = module.main_private_net_and_dns_zone.external_gateway_ip
}

# Module used

output "main_region" {
  value = module.common_variables.main_region
}

output "external_network" {
  value = module.common_variables.external_network
}

output "external_network_id" {
  value = module.common_variables.external_network.id
}

output "floating_ip_pools" {
  value = module.common_variables.floating_ip_pools

}

output "resolvers_ip" {
  value = module.common_variables.resolvers_ip
}

output "mtu_size" {
  value = module.common_variables.mtu_size
}

output "availability_zones_names" {
  value = module.common_variables.availability_zones_names
}

output "availability_zone_no_gpu_name" {
  value = module.common_variables.availability_zones_names.availability_zone_no_gpu
}

output "availability_zone_with_gpu_name" {
  value = module.common_variables.availability_zones_names.availability_zone_with_gpu
}

output "ssh_sources" {
  value = module.common_variables.ssh_sources
}

output "networks_with_d4s_services" {
  value = module.common_variables.networks_with_d4s_services
}

output "ubuntu_1804" {
  value = module.common_variables.ubuntu_1804
}

output "ubuntu_2204" {
  value = module.common_variables.ubuntu_2204
}

output "centos_7" {
  value = module.common_variables.centos_7
}

output "almalinux_9" {
  value = module.common_variables.almalinux_9
}

output "ubuntu1804_data_file" {
  value = module.common_variables.ubuntu1804_data_file
}

output "ubuntu2204_data_file" {
  value = module.common_variables.ubuntu2204_data_file
}

output "el7_data_file" {
  value = module.common_variables.el7_data_file
}

output "ssh_jump_proxy" {
  value = module.common_variables.ssh_jump_proxy
}

output "internal_ca_data" {
  value = module.common_variables.internal_ca_data
}

output "prometheus_server_data" {
  value = module.common_variables.prometheus_server_data
}

output "shared_postgresql_server_data" {
  value = module.common_variables.shared_postgresql_server_data
}

output "haproxy_l7_data" {
  value = module.common_variables.haproxy_l7_data
}

output "resource_registry_addresses" {
  value = module.common_variables.resource_registry_addresses
}

output "smartexecutor_addresses" {
  value = module.common_variables.smartexecutor_addresses
}

#Added by Francesco
output "policy_list" {
  value = module.common_variables.policy_list
}

#Added by Francesco
output "flavor_list" {
  value = module.common_variables.flavor_list
}

#Added by Francesco
output "security_group_list" {
  value = module.common_variables.security_group_list
}

#Added by Francesco
output "networks_list" {
  value = module.common_variables.networks_list
}
