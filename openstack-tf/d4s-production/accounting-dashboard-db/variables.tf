#Accounting Dashboard DB variables

variable "accounting_dashboard_db_data" {
  type = map(string)
  default = {
    name            = "accounting-dashboard-db-server"
    flavor          = "m1.medium"
    vol_data_name   = "accounting-dashboard-db-data"
    vol_data_size   = "10"
    vol_data_device = "/dev/vdb"
    server_ip       = "192.168.0.10"
  }
}

variable "accounting_dashoard_allowed_sources" {
  type = map(string)
  default = {
    "infrascience_net"     = "146.48.122.0/23"
    "google_datastudio1"   = "64.18.0.0/20"
    "google_datastudio2"   = "64.233.160.0/19"
    "google_datastudio3"   = "66.102.0.0/20"
    "google_datastudio4"   = "66.249.80.0/20"
    "google_datastudio5"   = "72.14.192.0/18"
    "google_datastudio6"   = "74.125.0.0/16"
    "google_datastudio7"   = "108.177.8.0/21"
    "google_datastudio8"   = "173.194.0.0/16"
    "google_datastudio9"   = "207.126.144.0/20"
    "google_datastudio10"  = "209.85.128.0/17"
    "google_datastudio11"  = "216.58.192.0/19"
    "google_datastudio12"  = "216.239.32.0/19"
    "google_datastudio13"  = "142.251.74.0/23"
    "openstack_production" = "146.48.31.57/32"
  }
}

