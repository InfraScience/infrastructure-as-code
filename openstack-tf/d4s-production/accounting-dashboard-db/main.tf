# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_networking_secgroup_v2" "accounting_dashboard_db_access_list" {
  name                 = "accounting_dashboard_db_access_list"
  delete_default_rules = "true"
  description          = "Allowed connections to the accounting dashboard database"
}

resource "openstack_networking_secgroup_rule_v2" "access_to_the_accounting_dashboard_db" {
  for_each          = toset([var.accounting_dashoard_allowed_sources.infrascience_net, var.accounting_dashoard_allowed_sources.google_datastudio1, var.accounting_dashoard_allowed_sources.google_datastudio2, var.accounting_dashoard_allowed_sources.google_datastudio3, var.accounting_dashoard_allowed_sources.google_datastudio4, var.accounting_dashoard_allowed_sources.google_datastudio5, var.accounting_dashoard_allowed_sources.google_datastudio6, var.accounting_dashoard_allowed_sources.google_datastudio7, var.accounting_dashoard_allowed_sources.google_datastudio8, var.accounting_dashoard_allowed_sources.google_datastudio9, var.accounting_dashoard_allowed_sources.google_datastudio10, var.accounting_dashoard_allowed_sources.google_datastudio11, var.accounting_dashoard_allowed_sources.google_datastudio12, var.accounting_dashoard_allowed_sources.google_datastudio13, var.accounting_dashoard_allowed_sources.openstack_production])
  security_group_id = openstack_networking_secgroup_v2.accounting_dashboard_db_access_list.id
  description       = "Access to the Accounting Dashboard DB"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = each.value
}

# Block device
resource "openstack_blockstorage_volume_v3" "accounting_dashboard_db_data_vol" {
  name = var.accounting_dashboard_db_data.vol_data_name
  size = var.accounting_dashboard_db_data.vol_data_size
}

#
# Ports in the timescaleDB network
resource "openstack_networking_port_v2" "accounting_dashboard_port_on_main_net" {
  name           = "accounting_dashboard_port_on_main_net"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.privnet_dns_router.outputs.main_subnet_network_id
  }
  security_group_ids = [
    openstack_networking_secgroup_v2.accounting_dashboard_db_access_list.id,
    data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id
  ]
}

# Instance
resource "openstack_compute_instance_v2" "accounting_dashboard_db_server" {
  name                    = var.accounting_dashboard_db_data.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = var.accounting_dashboard_db_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.name, data.terraform_remote_state.infrastructure_setup.outputs.access_postgresql_security_group.name, openstack_networking_secgroup_v2.accounting_dashboard_db_access_list.name]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = module.common_variables.networks_list.shared_postgresql
    fixed_ip_v4 = var.accounting_dashboard_db_data.server_ip
  }
  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "accounting_dashboard_db_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.accounting_dashboard_db_server.id
  volume_id   = openstack_blockstorage_volume_v3.accounting_dashboard_db_data_vol.id
  device      = var.accounting_dashboard_db_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.accounting_dashboard_db_server]
}

resource "openstack_compute_interface_attach_v2" "main_network_to_accounting_dashboard" {
  instance_id = openstack_compute_instance_v2.accounting_dashboard_db_server.id
  port_id     = openstack_networking_port_v2.accounting_dashboard_port_on_main_net.id
}
# Floating IP and DNS record
resource "openstack_networking_floatingip_v2" "accounting_dashboard_db_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "Accounting dashboard"
}

resource "openstack_networking_floatingip_associate_v2" "accounting_dashboard_db" {
  floating_ip = openstack_networking_floatingip_v2.accounting_dashboard_db_ip.address
  port_id     = openstack_networking_port_v2.accounting_dashboard_port_on_main_net.id
}

locals {
  accounting_dashboard_recordset_name = "accounting-dashboard-db.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "accounting_dashboard_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.accounting_dashboard_recordset_name
  description = "Public IP address of the Accounting Dashboard"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.accounting_dashboard_db_ip.address]
}

output "accounting_dashboard_public_ip_address" {
  value = openstack_networking_floatingip_v2.accounting_dashboard_db_ip.address
}

output "accounting_dashboard_hostname" {
  value = openstack_dns_recordset_v2.accounting_dashboard_recordset.name
}
