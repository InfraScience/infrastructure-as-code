#D4Science DB variables

variable "d4science_db_data" {
  type = map(string)
  default = {
    name            = "d4science-db"
    flavor          = "m1.large"
    vol_data_name   = "d4science-db-data"
    vol_data_size   = "600"
    vol_data_device = "/dev/vdb"
    vol_backup_name   = "d4science-db-backup"
    vol_backup_size   = "400"
    vol_backup_device = "/dev/vdc"
    server_ip       = "192.168.0.20"
  }
}

variable "d4science_db_allowed_sources" {
  type = map(string)
  default = {
    "public"     = "0.0.0.0/0"
  }
}

