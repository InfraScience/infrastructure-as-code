# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_networking_secgroup_v2" "d4science_db_access_list" {
  name                 = "d4science_db_access_list"
  delete_default_rules = "true"
  description          = "Allowed connections to the d4science database"
}

resource "openstack_networking_secgroup_rule_v2" "access_to_the_d4science_db" {
  for_each          = toset([var.d4science_db_allowed_sources.public])
  security_group_id = openstack_networking_secgroup_v2.d4science_db_access_list.id
  description       = "Access to the d4science db"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = each.value
}

# Block device
resource "openstack_blockstorage_volume_v3" "d4science_db_data_vol" {
  name = var.d4science_db_data.vol_data_name
  size = var.d4science_db_data.vol_data_size
}

resource "openstack_blockstorage_volume_v3" "d4science_db_backup_vol" {
  name = var.d4science_db_data.vol_backup_name
  size = var.d4science_db_data.vol_backup_size
}

#
# Ports in d4science db network
resource "openstack_networking_port_v2" "d4science_db_port_on_main_net" {
  name           = "d4science_db_port_on_main_net"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  admin_state_up = "true"
  fixed_ip {
    subnet_id = data.terraform_remote_state.privnet_dns_router.outputs.main_subnet_network_id
  }
  security_group_ids = [
    openstack_networking_secgroup_v2.d4science_db_access_list.id,
    data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id
  ]
}

# Instance
resource "openstack_compute_instance_v2" "d4science_db_server" {
  name                    = var.d4science_db_data.name
  #availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = var.d4science_db_data.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.name, data.terraform_remote_state.infrastructure_setup.outputs.access_postgresql_security_group.name, openstack_networking_secgroup_v2.d4science_db_access_list.name]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name        = module.common_variables.networks_list.shared_postgresql
    fixed_ip_v4 = var.d4science_db_data.server_ip
  }
  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "d4science_db_attach_data_vol" {
  instance_id = openstack_compute_instance_v2.d4science_db_server.id
  volume_id   = openstack_blockstorage_volume_v3.d4science_db_data_vol.id
  device      = var.d4science_db_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.d4science_db_server]
}

resource "openstack_compute_volume_attach_v2" "d4science_db_attach_backup_vol" {
  instance_id = openstack_compute_instance_v2.d4science_db_server.id
  volume_id   = openstack_blockstorage_volume_v3.d4science_db_backup_vol.id
  device      = var.d4science_db_data.vol_backup_device
  depends_on  = [openstack_compute_instance_v2.d4science_db_server]
}


resource "openstack_compute_interface_attach_v2" "main_network_to_d4science_db" {
  instance_id = openstack_compute_instance_v2.d4science_db_server.id
  port_id     = openstack_networking_port_v2.d4science_db_port_on_main_net.id
}
# Floating IP and DNS record
resource "openstack_networking_floatingip_v2" "d4science_db_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "D4Science DB"
}

resource "openstack_networking_floatingip_associate_v2" "d4science_db_associate_floatingip" {
  floating_ip = openstack_networking_floatingip_v2.d4science_db_ip.address
  port_id     = openstack_networking_port_v2.d4science_db_port_on_main_net.id
}

locals {
  d4science_db_recordset_name = "d4science-db.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "d4science_db_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.d4science_db_recordset_name
  description = "Public IP address of D4Science DB"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.d4science_db_ip.address]
}

output "d4science_db_public_ip_address" {
  value = openstack_networking_floatingip_v2.d4science_db_ip.address
}

output "d4science_db_hostname" {
  value = openstack_dns_recordset_v2.d4science_db_recordset.name
}
