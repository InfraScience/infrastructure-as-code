#variables

variable "logstash_data" {
  type = map(string)
  default = {
    name              = "logstash-itserr"
    flavor            = "m1.large"
    vol_data_name     = "logstash-data"
    vol_data_size     = "50"
    vol_data_device   = "/dev/vdb"
  }
}
