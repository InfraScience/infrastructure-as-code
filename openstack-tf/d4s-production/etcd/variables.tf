#variables

variable "etcd_data" {
  type = map(string)
  default = {
    name              = "etcd-itserr"
    flavor            = "c1.small"
    vol_data_name     = "etcd-data"
    vol_data_size     = "5"
    vol_data_device   = "/dev/vdb"
  }
}
