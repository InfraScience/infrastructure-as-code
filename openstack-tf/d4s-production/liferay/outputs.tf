output "liferay_data" {
  value = module.liferay.liferay_data
}

output "liferay_ip_addrs" {
  value = module.liferay.liferay_ip_addrs
}

output "liferay_recordsets" {
  value = module.liferay.liferay_recordsets
}

output "liferay_nfs_port_data" {
  value = module.liferay.nfs_port_data
}

output "liferay_nfs_volume_data" {
  value = module.liferay.liferay_nfs_volume_data
}

output "liferay_nfs_volume_acls" {
  value     = module.liferay.liferay_nfs_volume_acls
  sensitive = true
}
