octavia_swarm_data = {
  swarm_lb_name           = "d4s-production-cloud-swarm-l4"
  swarm_lb_description    = "L4 balancer that serves the D4Science production Docker Swarm cluster"
  swarm_lb_name           = "d4s-production-cloud-swarm-l4"
  octavia_flavor          = "octavia_amphora-mvcpu-ha"
  octavia_flavor_id       = "394988b5-6603-4a1e-a939-8e177c6681c7"
  swarm_lb_hostname       = "swarm-lb"
  swarm_octavia_main_ip   = "10.1.40.30"
  swarm_octavia_main_cidr = "10.1.40.30/32"
  # The following aren't available when the module runs so we have to get them with the command
  # openstack --os-cloud d4s-pre port list -f value | grep octavia-lb-vrrp
  # This means that the execution will fail
  octavia_vrrp_ip_1 = "10.1.43.97/32"
  octavia_vrrp_ip_2 = "10.1.44.78/32"
}
