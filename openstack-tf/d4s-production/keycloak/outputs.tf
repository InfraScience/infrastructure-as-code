output "keycloak_data" {
  value = module.keycloak.keycloak_data
}

output "keycloak_recordsets" {
  value = module.keycloak.keycloak_recordsets
}

output "keycloak_nfs_port_data" {
  value = module.keycloak.nfs_port_data
}

output "keycloak_nfs_volume_data" {
  value = module.keycloak.keycloak_nfs_volume_data
}

output "keycloak_nfs_volume_acls" {
  value     = module.keycloak.keycloak_nfs_volume_acls
  sensitive = true
}

