#Harbor  variables

variable "pwdns_data" {
  type = map(string)
  default = {
    name              = "powerdns"
    flavor            = "c1.small"
    object_store_name = "pwdns-registry"
  }
}
