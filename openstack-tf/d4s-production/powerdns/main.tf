# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "pwdns_access_list" {
  name                 = "pwdns_access_list"
  delete_default_rules = "true"
  description          = "Allowed connections to the powerdns server"
}

resource "openstack_networking_secgroup_rule_v2" "tcp_access_to_pwdns" {
  security_group_id = openstack_networking_secgroup_v2.pwdns_access_list.id
  description       = "TCP access to the PowerDNS Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "upd_access_to_pwdns" {
  security_group_id = openstack_networking_secgroup_v2.pwdns_access_list.id
  description       = "UDP access to the PowerDNS Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "http_access_to_pwdns" {
  security_group_id = openstack_networking_secgroup_v2.pwdns_access_list.id
  description       = "HTTP access to the PowerDNS Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "https_access_to_pwdns" {
  security_group_id = openstack_networking_secgroup_v2.pwdns_access_list.id
  description       = "HTTPS access to the PowerDNS Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
}


# Instance
resource "openstack_compute_instance_v2" "pwdns_server" {
  name = var.pwdns_data.name
  flavor_name     = var.pwdns_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name
  security_groups = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.name, openstack_networking_secgroup_v2.pwdns_access_list.name]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
      name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_networking_floatingip_v2" "pwdns_ip" {
  pool = data.terraform_remote_state.privnet_dns_router.outputs.floating_ip_pools.main_public_ip_pool
  # The DNS association does not work because of a bug in the OpenStack API
  description = "PowerDNS ip"
}

resource "openstack_compute_floatingip_associate_v2" "pwdns_floatingip_associate" {
  instance_id = openstack_compute_instance_v2.pwdns_server.id
  floating_ip = openstack_networking_floatingip_v2.pwdns_ip.address
  fixed_ip = openstack_compute_instance_v2.pwdns_server.network[0].fixed_ip_v4
}



locals {
  pwdns_recordset_name = "powerdns.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "pwdns_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.pwdns_recordset_name
  description = "Public IP address of the PowerDNS Server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.pwdns_ip.address]
}

output "pwdns_public_ip_address" {
  value = openstack_networking_floatingip_v2.pwdns_ip.address
}

output "pwdns_hostname" {
  value = openstack_dns_recordset_v2.pwdns_recordset.name
}
