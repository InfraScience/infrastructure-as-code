
# Instance volume.
# One for the data in /var/lib/mysql
resource "openstack_blockstorage_volume_v3" "d4s_web_mysql_server_volume" {
  name = "d4s-web-mysql-server-vol"
  size = 40
}

# Security groups
# One to allow access to the mysql/mariadb port
resource "openstack_networking_secgroup_v2" "d4s_web_mysql_server_secgroup" {
  name                 = "d4s-web-mysql-server-secgroup"
  delete_default_rules = "true"
  description          = "Security group for traffic TO the D4science website MySQL server"
}

resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = module.common_variables.shared_postgresql_server_data.network_cidr
}
resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_trust_it_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "93.149.58.242/32"
}
resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_trust_it_2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "52.51.15.59/32"
}
resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_vpn_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "146.48.122.27/32"
}
resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_vpn_2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = "146.48.122.49/32"
}

resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_main_priv_net" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_s2i2s_vpn_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.ssh_sources.s2i2s_vpn_1_cidr
}

resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_s2i2s_vpn_2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.ssh_sources.s2i2s_vpn_2_cidr
}

resource "openstack_networking_secgroup_rule_v2" "d4s_web_mysql_server_secgroup_rule_icmp_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
}

# Port in the main private network
resource "openstack_networking_port_v2" "d4s_web_mysql_server_port" {
  name               = "d4s-web-mysql-server-port"
  admin_state_up     = true
  network_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id, openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id]
}

# Instance
resource "openstack_compute_instance_v2" "d4s_web_mysql_server" {
  name                    = var.d4science_website_mysql_resources.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = var.d4science_website_mysql_resources.flavor
  key_pair                = module.ssh_settings.ssh_key_name

  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    port = openstack_networking_port_v2.d4s_web_mysql_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "d4s_web_mysql_server_volume_attach" {
  instance_id = openstack_compute_instance_v2.d4s_web_mysql_server.id
  volume_id   = openstack_blockstorage_volume_v3.d4s_web_mysql_server_volume.id
  device      = "/dev/vdb"
}

# Port that talks to the shared postgresql service
resource "openstack_networking_port_v2" "d4s_web_mysql_postgresql_port" {
  name           = "d4s-web-mysql-postgresql-port"
  network_id     = data.terraform_remote_state.infrastructure_setup.outputs.shared_postgresql_network_data.id
  admin_state_up = "true"
}

resource "openstack_networking_port_secgroup_associate_v2" "d4s_web_mysql_postgresql_port_secgroup" {
  port_id            = openstack_networking_port_v2.d4s_web_mysql_postgresql_port.id
  security_group_ids = [openstack_networking_secgroup_v2.d4s_web_mysql_server_secgroup.id]
}

resource "openstack_compute_interface_attach_v2" "d4s_web_mysql_postgresql_interface" {
  instance_id = openstack_compute_instance_v2.d4s_web_mysql_server.id
  port_id     = openstack_networking_port_v2.d4s_web_mysql_postgresql_port.id
}


# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "d4s_web_mysql_floating_ip" {
  pool        = data.terraform_remote_state.infrastructure_setup.outputs.floating_ip_pools.main_public_ip_pool
  description = "Public IP address for the Uptime Kuma Server"
}

# Associate the floating IP to the instance
resource "openstack_networking_floatingip_associate_v2" "d4s_web_mysql_ip" {
  floating_ip = openstack_networking_floatingip_v2.d4s_web_mysql_floating_ip.address
  port_id     = openstack_networking_port_v2.d4s_web_mysql_server_port.id
}

locals {
  d4s_web_mysql_recordset_name = "d4s-web-mysql.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

resource "openstack_dns_recordset_v2" "d4s_web_mysql_recordset" {
  zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
  name        = local.d4s_web_mysql_recordset_name
  description = "Public IP address of the Uptime Kuma Server"
  ttl         = 8600
  type        = "A"
  records     = [openstack_networking_floatingip_v2.d4s_web_mysql_floating_ip.address]
}

output "d4s_web_mysql_public_ip_address" {
  value = openstack_networking_floatingip_v2.d4s_web_mysql_floating_ip.address
}

output "d4s_web_mysql_hostname" {
  value = openstack_dns_recordset_v2.d4s_web_mysql_recordset.name
}
