# D4Science website MySQL resources

variable "d4science_website_mysql_resources" {
  type = map(string)
  default = {
    name   = "d4science-website-mysql"
    flavor = "m1.medium"
  }
}
