#variables

variable "qdrant_data" {
  type = map(string)
  default = {
    name              = "qdrant"
    flavor            = "m2.large"
    vol_data_name     = "data"
    vol_data_size     = "500"
    vol_data_device   = "/dev/vdb"
  }
}

