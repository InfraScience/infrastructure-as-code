# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "qdrant_server_secgroup" {
  name                 = "qdrant-server-secgroup"
  delete_default_rules = "true"
  description          = "Allowed connections to the qdrant server"
}

resource "openstack_networking_secgroup_rule_v2" "rest_api_qdrant_server" {
  security_group_id = openstack_networking_secgroup_v2.qdrant_server_secgroup.id
  description       = "REST API access to the qdrant Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6333
  port_range_max    = 6333
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "grpc_qdrant_server" {
  security_group_id = openstack_networking_secgroup_v2.qdrant_server_secgroup.id
  description       = "gRPC access to the qdrant Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6334
  port_range_max    = 6334
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

# Port in the main private network
resource "openstack_networking_port_v2" "qdrant_server_port" {
  name               = "qdrant-server-port"
  admin_state_up     = true
  network_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id, openstack_networking_secgroup_v2.qdrant_server_secgroup.id]
}


# Block device
resource "openstack_blockstorage_volume_v3" "qdrant_data_vol" {
  name = var.qdrant_data.vol_data_name
  size = var.qdrant_data.vol_data_size
}


# Instance
resource "openstack_compute_instance_v2" "qdrant_server" {
  name = var.qdrant_data.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name     = var.qdrant_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
      port = openstack_networking_port_v2.qdrant_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "qdrant_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.qdrant_server.id
  volume_id   = openstack_blockstorage_volume_v3.qdrant_data_vol.id
  device      = var.qdrant_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.qdrant_server]
}



