# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    sdi1 = {
      name              = "sdi1",
      description       = "This instance is a SDI service",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 10
    }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    sdi1 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["sdi1", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "SDI 1 Production"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
