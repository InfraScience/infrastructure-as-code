#variables

variable "rabbitmq_data" {
  type = map(string)
  default = {
    name              = "rabbitmq-itserr"
    flavor            = "c1.small"
  }
}
