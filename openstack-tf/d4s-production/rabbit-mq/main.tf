# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "rabbitmq_server_secgroup" {
  name                 = "rabbitmq_access_list"
  delete_default_rules = "true"
  description          = "Allowed connections to the rabbitmq server"
}

resource "openstack_networking_secgroup_rule_v2" "amqp_rabbitmq_server" {
  security_group_id = openstack_networking_secgroup_v2.rabbitmq_server_secgroup.id
  description       = "AMQP access to the rabbitmq Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5672
  port_range_max    = 5672
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "webui_rabbitmq_server" {
  security_group_id = openstack_networking_secgroup_v2.rabbitmq_server_secgroup.id
  description       = "WEB UI access to the rabbitmq Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 15672
  port_range_max    = 15672
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

# Port in the main private network
resource "openstack_networking_port_v2" "rabbitmq_server_port" {
  name               = "rabbitmq-server-port"
  admin_state_up     = true
  network_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id, openstack_networking_secgroup_v2.rabbitmq_server_secgroup.id]
}

# Instance
resource "openstack_compute_instance_v2" "rabbitmq_server" {
  name = var.rabbitmq_data.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name     = var.rabbitmq_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
      port = openstack_networking_port_v2.rabbitmq_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data
    ]
  }
}
