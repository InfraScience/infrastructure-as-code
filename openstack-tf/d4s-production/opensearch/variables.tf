#variables

variable "opensearch_data" {
  type = map(string)
  default = {
    name              = "opensearch-itserr"
    flavor            = "m1.large"
    vol_data_name     = "opensearch-data"
    vol_data_size     = "50"
    vol_data_device   = "/dev/vdb"
  }
}
