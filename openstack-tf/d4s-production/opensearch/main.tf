# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "infrastructure_setup" {
  backend = "local"

  config = {
    path = "../basic-infrastructure/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "opensearch_secgroup" {
  name                 = "opensearch_itserr_secgroup"
  delete_default_rules = "true"
  description          = "Allowed connections to the stack OpenSearch server"
}

resource "openstack_networking_secgroup_rule_v2" "rest_api_opensearch_server" {
  security_group_id = openstack_networking_secgroup_v2.opensearch_secgroup.id
  description       = "REST API access to the OpenSearch Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9200
  port_range_max    = 9200
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "cluster_opensearch_server" {
  security_group_id = openstack_networking_secgroup_v2.opensearch_secgroup.id
  description       = "Cluster access to the OpenSearch Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9300
  port_range_max    = 9300
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "dashboard_opensearch_server" {
  security_group_id = openstack_networking_secgroup_v2.opensearch_secgroup.id
  description       = "Dashboard access to the OpenSearch Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5601
  port_range_max    = 5601
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "perf_opensearch_server" {
  security_group_id = openstack_networking_secgroup_v2.opensearch_secgroup.id
  description       = "Performance Analyzer access to the OpenSearch Server"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9600
  port_range_max    = 9600
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

# Port in the main private network
resource "openstack_networking_port_v2" "opensearch_server_port" {
  name               = "opensearch-itserr-server-port"
  admin_state_up     = true
  network_id         = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network_id
  security_group_ids = [data.terraform_remote_state.infrastructure_setup.outputs.default_security_group.id, openstack_networking_secgroup_v2.opensearch_secgroup.id]
}

# Block device
resource "openstack_blockstorage_volume_v3" "opensearch_data_vol" {
  name = var.opensearch_data.vol_data_name
  size = var.opensearch_data.vol_data_size
}

# Instance
resource "openstack_compute_instance_v2" "opensearch_server" {
  name = var.opensearch_data.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name     = var.opensearch_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name

  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 20
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
      port = openstack_networking_port_v2.opensearch_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data
    ]
  }
}


resource "openstack_compute_volume_attach_v2" "opensearch_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.opensearch_server.id
  volume_id   = openstack_blockstorage_volume_v3.opensearch_data_vol.id
  device      = var.opensearch_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.opensearch_server]
}
