resource "google_dns_managed_zone" "openaire-gcp" {
  name        = "openaire-gcp"
  dns_name    = "openaire-gcp.d4science.org."
  description = "Zone for the OpenAIRE GCP resources, mostly OKD."
  labels = {
    name = "openaire-okd"
  }
}
