# How to obtain the "generated" data

We can import resources using the `terraform import command. The terraform tasks must be already defined for the import to be successful.
The information required is:

* private network id
* private subnet id
* external router id
* router interface id (it's the "port_id" value in the field "interfaces_info")

```shell-session
openstack --os-cloud garr-na network list
openstack --os-cloud garr-na subnet list
openstack --os-cloud garr-na router list
openstack --os-cloud garr-na router show isti-router
```

Those resources have been imported running:

```shell-session
terraform import openstack_networking_network_v2.main-private-network a326a836-6883-43ef-9383-64f0876b0a25
terraform import openstack_networking_subnet_v2.main-private-subnet dd42322a-a4c4-4364-9d23-1e9806e9b9fa
terraform import openstack_networking_router_v2.external-router 53cddad1-4593-47da-af8c-26530ce84460
terraform import openstack_networking_router_interface_v2.private-network-routing be45f599-e8aa-4e4f-b6e4-ed7db3e8d3be
```

