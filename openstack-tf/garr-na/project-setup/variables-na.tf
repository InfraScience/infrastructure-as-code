#
# The project is the same for all the GARR regions
variable "os_project_data" {
  type = map(string)
  default = {
    id = "a2de533851354b1f8d99ac6b6216d92e"
  }
}

variable "os_region_data" {
  type = map(string)
  default = {
    name = "garr-na"
  }
}

variable "default_security_group" {
  type = map(string)
  default = {
    name = "default"
    id   = "b1dca1a8-4eb4-43b0-9fe1-e6a399e23fbb"
  }
}

variable "availability_zone" {
  type = map(string)
  default = {
    name = "nova"
  }
}

variable "main_private_network" {
  type = map(string)
  default = {
    name        = "isti-VM-NA-net"
    description = "GARR NA main network"
    id          = "a326a836-6883-43ef-9383-64f0876b0a25"
  }
}

variable "mtu_size" {
  default = 9000
}

variable "main_private_subnet" {
  type = map(string)
  default = {
    name             = "isti-na-subnet"
    description      = "GARR-NA main subnet"
    cidr             = "192.168.102.0/24"
    gateway_ip       = "192.168.102.1"
    allocation_start = "192.168.102.2"
    allocation_end   = "192.168.102.250"
    id               = "dd42322a-a4c4-4364-9d23-1e9806e9b9fa"
  }
}

variable "resolvers_ip" {
  type    = list(string)
  default = ["193.206.141.38", "193.206.141.42"]
}

variable "external_router" {
  type = map(string)
  default = {
    name        = "isti-router"
    description = "GARR-NA main router"
  }
}

variable "external_network" {
  type = map(string)
  default = {
    name = "floating-ip"
    id   = "8a14fb84-2ab4-4114-a95e-1acc029850ff"
  }
}

variable "jump_proxy_ssh_shell" {
  type = map(string)
  default = {
    hostname   = "shell.garr-na1.d4science.org"
    private_ip = "192.168.102.151"
    cidr       = "192.168.102.151/24"
    public_ip  = "90.147.152.217"
  }
}

variable "prometheus_host" {
  type = map(string)
  default = {
    hostname   = "prometheus-na1.garr.d4science.net"
    private_ip = "192.168.102.12"
    public_ip  = "90.147.152.45"
  }
}
