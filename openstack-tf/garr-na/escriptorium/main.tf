#
# https://support.d4science.org/issues/28405
# https://gitlab.com/scripta/escriptorium/
#
# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  # source = "../../modules/common_variables"
  source = "../../modules/garr_common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_blockstorage_volume_v3" "escriptorium_vm_vol" {
  name = "eScriptorium VM, data for the Docker stuff"
  size = 30
}

resource "openstack_blockstorage_volume_v3" "escriptorium_data_vol" {
  name = "eScriptorium data volume"
  size = 50
}

resource "openstack_blockstorage_volume_v3" "escriptorium_media_vol" {
  name = "eScriptorium media volume"
  size = 50
}

resource "openstack_networking_port_v2" "escriptorium_ip_in_main_net" {
  name           = "escriptorium_main_interface"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id,
  ]
}

resource "openstack_compute_instance_v2" "escriptorium_server" {
  name                    = "escriptorium-service"
  availability_zone_hints = "nova"
  flavor_name             = "m3.large"
  key_pair                = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.escriptorium_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "escriptorium_docker_attach_vol" {
  instance_id = openstack_compute_instance_v2.escriptorium_server.id
  volume_id   = openstack_blockstorage_volume_v3.escriptorium_vm_vol.id
  device      = "/dev/vdb"
}

resource "openstack_compute_volume_attach_v2" "escriptorium_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.escriptorium_server.id
  volume_id   = openstack_blockstorage_volume_v3.escriptorium_data_vol.id
  device      = "/dev/vdc"
}

resource "openstack_compute_volume_attach_v2" "escriptorium_media_attach_vol" {
  instance_id = openstack_compute_instance_v2.escriptorium_server.id
  volume_id   = openstack_blockstorage_volume_v3.escriptorium_media_vol.id
  device      = "/dev/vdd"
}

resource "openstack_networking_floatingip_v2" "escriptorium_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "eScriptorium test server"
}

resource "openstack_networking_floatingip_associate_v2" "escriptorium_server" {
  floating_ip = openstack_networking_floatingip_v2.escriptorium_floating_ip.address
  port_id     = openstack_networking_port_v2.escriptorium_ip_in_main_net.id
}

# Ingress to the Postgresql port
resource "openstack_networking_secgroup_v2" "escriptorium_postgresql_access" {
  name                 = "access_to_the_escriptorium_postgresql_service"
  delete_default_rules = "true"
  description          = "Access the eScriptorium PostgreSQL service"
}

resource "openstack_networking_secgroup_rule_v2" "escriptorium_postgresql_access_from_the_private_network" {
  security_group_id = openstack_networking_secgroup_v2.escriptorium_postgresql_access.id
  description       = "Allow connections to port 5432 from the 192.168.102.0/24 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "escriptorium_postgresql_access_from_the_infrascience_network" {
  security_group_id = openstack_networking_secgroup_v2.escriptorium_postgresql_access.id
  description       = "Allow connections to port 5432 from the 146.48.122.0/23 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "146.48.122.0/23"
}

resource "openstack_networking_secgroup_rule_v2" "escriptorium_postgresql_access_from_the_s2i2s_network" {
  security_group_id = openstack_networking_secgroup_v2.escriptorium_postgresql_access.id
  description       = "Allow connections to port 5432 from the 146.48.28.0/22 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "146.48.28.0/22"
}

resource "openstack_blockstorage_volume_v3" "escriptorium_pg_test_vol" {
  name = "eScriptorium test postgresql data"
  size = 10
}

resource "openstack_networking_port_v2" "escriptorium_pg_test_ip_in_main_net" {
  name           = "escriptorium_postgres_test_main_interface"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id, openstack_networking_secgroup_v2.escriptorium_postgresql_access.id
  ]
}

resource "openstack_compute_instance_v2" "escriptorium_pg_test_server" {
  name                    = "escriptorium-postgresql-test-service"
  availability_zone_hints = "nova"
  flavor_name             = "m1.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.escriptorium_pg_test_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "escriptorium_pg_test_attach_pg_vol" {
  instance_id = openstack_compute_instance_v2.escriptorium_pg_test_server.id
  volume_id   = openstack_blockstorage_volume_v3.escriptorium_pg_test_vol.id
  device      = "/dev/vdb"
}

resource "openstack_networking_floatingip_v2" "escriptorium_pg_test_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "eScriptorium postgresql test server"
}

resource "openstack_networking_floatingip_associate_v2" "escriptorium_pg_server" {
  floating_ip = openstack_networking_floatingip_v2.escriptorium_pg_test_floating_ip.address
  port_id     = openstack_networking_port_v2.escriptorium_pg_test_ip_in_main_net.id
}
