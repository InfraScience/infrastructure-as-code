output "escriptorium_instance" {
  value     = openstack_compute_instance_v2.escriptorium_server
  sensitive = true
}

output "escriptorium_floating_ip" {
  value = openstack_networking_floatingip_v2.escriptorium_floating_ip
}

output "escriptorium_pg_test_instance" {
  value     = openstack_compute_instance_v2.escriptorium_pg_test_server
  sensitive = true
}

output "escriptorium_pg_test_floating_ip" {
  value = openstack_networking_floatingip_v2.escriptorium_pg_test_floating_ip
}
