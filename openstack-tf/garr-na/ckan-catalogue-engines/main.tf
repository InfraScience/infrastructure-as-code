# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  # source = "../../modules/common_variables"
  source = "../../modules/garr_common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# External SSH accesses
resource "openstack_networking_secgroup_v2" "ckan_test_access" {
  name                 = "ssh_access_to_the_ckan_test_service"
  delete_default_rules = "true"
  description          = "SSH Access to the CKAN test service"
}

resource "openstack_networking_secgroup_rule_v2" "ckan_test_access_from_trust_it_public_ip" {
  security_group_id = openstack_networking_secgroup_v2.ckan_test_access.id
  description       = "Allow SSH connections to the CKAN test server from 93.149.58.242/32"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "93.149.58.242/32"
}

resource "openstack_networking_secgroup_rule_v2" "ckan_test_access_from_trust_it_vpn" {
  security_group_id = openstack_networking_secgroup_v2.ckan_test_access.id
  description       = "Allow SSH connections to the CKAN test server from 52.51.15.59/32"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "52.51.15.59/32"
}

resource "openstack_blockstorage_volume_v3" "ckan_test_vm_vol" {
  name = "CKAN test VM, data for the Docker stuff"
  size = 30
}

resource "openstack_networking_port_v2" "ckan_test_ip_in_main_net" {
  name           = "ckan_test_main_interface"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id, openstack_networking_secgroup_v2.ckan_test_access.id
  ]
}

resource "openstack_compute_instance_v2" "ckan_test_server" {
  name                    = "ckan-test-service"
  availability_zone_hints = "nova"
  flavor_name             = "m1.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.name, openstack_networking_secgroup_v2.ckan_test_access.name
  ]
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.ckan_test_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "ckan_test_docker_attach_vol" {
  instance_id = openstack_compute_instance_v2.ckan_test_server.id
  volume_id   = openstack_blockstorage_volume_v3.ckan_test_vm_vol.id
  device      = "/dev/vdb"
}

resource "openstack_networking_floatingip_v2" "ckan_test_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "CKAN test server"
}

resource "openstack_networking_floatingip_associate_v2" "ckan_test_server" {
  floating_ip = openstack_networking_floatingip_v2.ckan_test_floating_ip.address
  port_id     = openstack_networking_port_v2.ckan_test_ip_in_main_net.id
}

# Ingress to the Postgresql port
resource "openstack_networking_secgroup_v2" "ckan_postgresql_access" {
  name                 = "access_to_the_ckan_postgresql_service"
  delete_default_rules = "true"
  description          = "Access the CKAN PostgreSQL service"
}

resource "openstack_networking_secgroup_rule_v2" "ckan_postgresql_access_from_the_private_network" {
  security_group_id = openstack_networking_secgroup_v2.ckan_postgresql_access.id
  description       = "Allow connections to port 5432 from the 192.168.102.0/24 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr
}

resource "openstack_networking_secgroup_rule_v2" "ckan_postgresql_access_from_the_infrascience_network" {
  security_group_id = openstack_networking_secgroup_v2.ckan_postgresql_access.id
  description       = "Allow connections to port 5432 from the 146.48.122.0/23 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "146.48.122.0/23"
}

resource "openstack_networking_secgroup_rule_v2" "ckan_postgresql_access_from_the_s2i2s_network" {
  security_group_id = openstack_networking_secgroup_v2.ckan_postgresql_access.id
  description       = "Allow connections to port 5432 from the 146.48.28.0/22 network"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "146.48.28.0/22"
}

resource "openstack_blockstorage_volume_v3" "ckan_pg_test_vol" {
  name = "CKAN test postgresql data"
  size = 10
}

resource "openstack_networking_port_v2" "ckan_pg_test_ip_in_main_net" {
  name           = "ckan_postgres_test_main_interface"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id, openstack_networking_secgroup_v2.ckan_postgresql_access.id
  ]
}

resource "openstack_compute_instance_v2" "ckan_pg_test_server" {
  name                    = "ckan-postgres-test-service"
  availability_zone_hints = "nova"
  flavor_name             = "m1.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.ckan_pg_test_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "ckan_pg_test_attach_pg_vol" {
  instance_id = openstack_compute_instance_v2.ckan_pg_test_server.id
  volume_id   = openstack_blockstorage_volume_v3.ckan_pg_test_vol.id
  device      = "/dev/vdb"
}

resource "openstack_networking_floatingip_v2" "ckan_pg_test_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "CKAN postgresql test server"
}

resource "openstack_networking_floatingip_associate_v2" "ckan_pg_server" {
  floating_ip = openstack_networking_floatingip_v2.ckan_pg_test_floating_ip.address
  port_id     = openstack_networking_port_v2.ckan_pg_test_ip_in_main_net.id
}

resource "openstack_blockstorage_volume_v3" "ckan_solr_test_vol" {
  name = "CKAN test SOLR data"
  size = 10
}

resource "openstack_networking_port_v2" "ckan_solr_test_ip_in_main_net" {
  name           = "ckan_solr_test_main_interface"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id,
  ]
}

resource "openstack_compute_instance_v2" "ckan_solr_test_server" {
  name                    = "ckan-solr-test-service"
  availability_zone_hints = "nova"
  flavor_name             = "m1.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.ckan_solr_test_ip_in_main_net.id
  }

  user_data = file("${module.common_variables.ubuntu2404_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "ckan_solr_test_attach_pg_vol" {
  instance_id = openstack_compute_instance_v2.ckan_solr_test_server.id
  volume_id   = openstack_blockstorage_volume_v3.ckan_solr_test_vol.id
  device      = "/dev/vdb"
}

resource "openstack_networking_floatingip_v2" "ckan_solr_test_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "CKAN SOLR test server"
}

resource "openstack_networking_floatingip_associate_v2" "ckan_solr_test_server" {
  floating_ip = openstack_networking_floatingip_v2.ckan_solr_test_floating_ip.address
  port_id     = openstack_networking_port_v2.ckan_solr_test_ip_in_main_net.id
}
