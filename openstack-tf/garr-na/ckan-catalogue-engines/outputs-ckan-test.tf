output "ckan_test_instance" {
  value     = openstack_compute_instance_v2.ckan_test_server
  sensitive = true
}

output "ckan_test_floating_ip" {
  value = openstack_networking_floatingip_v2.ckan_test_floating_ip
}

output "ckan_pg_test_instance" {
  value     = openstack_compute_instance_v2.ckan_pg_test_server
  sensitive = true
}

output "ckan_pg_test_floating_ip" {
  value = openstack_networking_floatingip_v2.ckan_pg_test_floating_ip
}

output "ckan_solr_test_instance" {
  value     = openstack_compute_instance_v2.ckan_solr_test_server
  sensitive = true
}

output "ckan_solr_test_floating_ip" {
  value = openstack_networking_floatingip_v2.ckan_solr_test_floating_ip
}
