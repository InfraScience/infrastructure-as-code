#
# https://support.d4science.org/issues/28561
# https://hub.docker.com/r/openlink/virtuoso-opensource-7

# Security groups
# Allow access to the Virtuoso server from the outside
resource "openstack_networking_secgroup_v2" "grsf_virtuoso_secgroup" {
  name        = "grsf_virtuoso_secgroup"
  delete_default_rules = "true"
  description = "Security group for the GRSF Virtuoso server"
}

resource "openstack_networking_secgroup_rule_v2" "grsf_virtuoso_secgroup_rule_sql_s2i2s" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id
  protocol          = "tcp"
  port_range_min    = 1111
  port_range_max    = 1111
  remote_ip_prefix  = "146.48.28.0/22"
}

resource "openstack_networking_secgroup_rule_v2" "grsf_virtuoso_secgroup_rule_sql_yannis" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id
  protocol          = "tcp"
  port_range_min    = 1111
  port_range_max    = 1111
  remote_ip_prefix  = "139.91.183.81/32"
}

resource "openstack_networking_secgroup_rule_v2" "grsf_virtuoso_secgroup_rule_http_s2i2s" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id
  protocol          = "tcp"
  port_range_min    = 8890
  port_range_max    = 8890
  remote_ip_prefix  = "146.48.28.0/22"
}

resource "openstack_networking_secgroup_rule_v2" "grsf_virtuoso_secgroup_rule_sql" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id
  protocol          = "tcp"
  port_range_min    = 1111
  port_range_max    = 1111
  remote_ip_prefix  = "146.48.122.0/23"
}

resource "openstack_networking_secgroup_rule_v2" "grsf_virtuoso_secgroup_rule_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id
  protocol          = "tcp"
  port_range_min    = 8890
  port_range_max    = 8890
  remote_ip_prefix  = "146.48.122.0/23"
}

# Instance volumes.
# One for the docker stuff in /var/lib/docker.
resource "openstack_blockstorage_volume_v3" "grsf_virtuoso_docker_volume" {
  name = "grsf-virtuoso-docker-vol"
  size = 30
}
# One for the data in /database
resource "openstack_blockstorage_volume_v3" "grsf_virtuoso_database_volume" {
  name = "grsf-virtuoso-database-vol"
  size = 30
}

# Port in the main private network
resource "openstack_networking_port_v2" "grsf_virtuoso_server_port" {
  name           = "grsf-virtuoso-server-port"
  admin_state_up = "true"
  network_id     = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.id
  security_group_ids = [
    data.terraform_remote_state.privnet_dns_router.outputs.default_security_group.id,openstack_networking_secgroup_v2.grsf_virtuoso_secgroup.id]
}

# Instance
resource "openstack_compute_instance_v2" "grsf_virtuoso_server" {
  name                    = "grsf-virtuoso"
  availability_zone_hints = "nova"
  flavor_name             = "d1.large"
  key_pair                = module.ssh_settings.ssh_key_name

  block_device {
    uuid                  = module.common_variables.ubuntu_2404.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    port = openstack_networking_port_v2.grsf_virtuoso_server_port.id
  }

  user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


# Attach the volumes to the instance
resource "openstack_compute_volume_attach_v2" "grsf_virtuoso_docker_volume_attach" {
  instance_id = openstack_compute_instance_v2.grsf_virtuoso_server.id
  volume_id   = openstack_blockstorage_volume_v3.grsf_virtuoso_docker_volume.id
  device      = "/dev/vdc"
}

resource "openstack_compute_volume_attach_v2" "grsf_virtuoso_database_volume_attach" {
  instance_id = openstack_compute_instance_v2.grsf_virtuoso_server.id
  volume_id   = openstack_blockstorage_volume_v3.grsf_virtuoso_database_volume.id
  device      = "/dev/vdb"
}

# Allocate a floating IP
resource "openstack_networking_floatingip_v2" "grsf_virtuoso_floating_ip" {
  pool        = data.terraform_remote_state.privnet_dns_router.outputs.external_network.name
  description = "Public IP address for the GRSF Virtuoso Server"
}

# Associate the floating IP to the instance
resource "openstack_networking_floatingip_associate_v2" "grsf_virtuoso_ip" {
  floating_ip = openstack_networking_floatingip_v2.grsf_virtuoso_floating_ip.address
  port_id     = openstack_networking_port_v2.grsf_virtuoso_server_port.id
}

output "grsf_virtuoso_public_ip_address" {
  value = openstack_networking_floatingip_v2.grsf_virtuoso_floating_ip.address
}
