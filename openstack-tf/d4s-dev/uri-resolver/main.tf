# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}
#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module ssh_settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


# Instance one of uri-resolver (created without terraform, see #25695) 
# Used 'terraform import {resource_name} {resource_id}' to create TF state, see #28721 
resource "openstack_compute_instance_v2" "data" {
  name                    = "data"
  availability_zone = module.common_variables.availability_zone_with_gpu_name
  flavor_name             = "m1.medium"
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }


  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


# Instance 2 of Uri-Resolver created by module.
module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    data2 = {
      name              = "data2",
      description       = "This instance is a Uri Resolver service",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 20
    }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    data = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["data", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Uri Resolver Development 1"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    data2 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["data2", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Uri Resolver Development 2"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
