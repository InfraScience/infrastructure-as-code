# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


#
# Creates the server group "ckan-catalogue", NB but I'm not using it!!!!
#
resource "openstack_compute_servergroup_v2" "ckan-catalogue" {
  name     = "ckan-catalogue"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}


#
# Postgres instances via "postgres" module
#
module "instance_postgres_via_module" {
  source = "../../modules/postgres"

  # Postgres networking configuration.
  # NB. use this configuration by settings properly all the key fields 
  # postgres_networking_data = {
  #   description                    = "Data for the PostgreSQL server, including network CIDR and server IP"
  #   networking_security_group_name = "my network security name"
  #   network_cidr                   = "192.168.0.0/22"
  #   server_ip                      = "192.168.0.5"
  #   network_name                   = "the_network_name"
  #   server_cidr                    = "192.168.0.5/22"
  #   port_range_min                 = 5432
  #   port_range_max                 = 5432
  # }

  # Postgres instance
  postgres_instance_data = {
    postgres-ckan-dev = {
      name              = "postgres-ckan-dev",
      description       = "The Postgres ckan-dev instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_2204,
      volume = {
        name   = "postgres-ckan-dev_data_volume",
        size   = "20",
        device = "/dev/vdb",
      }
    }
  }
}


#
# Solr instances via "instance_with_data_volume" module
#
module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    solr-ckan-dev = {
      name              = "solr-ckan-dev",
      description       = "The Solr ckan-dev instance",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_2204,
      volume = {
        name   = "solr-ckan-dev_data_volume",
        size   = "20",
        device = "/dev/vdb"
      }
    }
  }
}
