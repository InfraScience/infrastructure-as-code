
# Default instances without data volume is EMPTY. Override it to create a proper instance plan
variable "ckan_instances" {
  type    = list(string)
  default = ["ckan-dev"]
}
