#Harbor  variables

variable "ollama_data" {
  type = map(string)
  default = {
    name              = "ollama"
    flavor            = "c.vgpu16"
    vol_data_name     = "ollama-data"
    vol_data_size     = "80"
    vol_data_device   = "/dev/vdc"
  }
}
