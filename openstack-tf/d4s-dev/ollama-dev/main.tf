# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Block device
resource "openstack_blockstorage_volume_v3" "ollama_data_vol" {
  name = var.ollama_data.vol_data_name
  size = var.ollama_data.vol_data_size
}



# Instance
resource "openstack_compute_instance_v2" "ollama_dev" {
  name = var.ollama_data.name
  availability_zone_hints = module.common_variables.availability_zone_with_gpu_name
  flavor_name     = var.ollama_data.flavor
  key_pair        = module.ssh_settings.ssh_key_name
  security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_2204.uuid
    source_type           = "image"
    volume_size           = 80
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }


 user_data = file("${module.common_variables.ubuntu2204_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


resource "openstack_compute_volume_attach_v2" "ollama_data_attach_vol" {
  instance_id = openstack_compute_instance_v2.ollama_dev.id
  volume_id   = openstack_blockstorage_volume_v3.ollama_data_vol.id
  device      = var.ollama_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.ollama_dev]
}


#
# Creates the server group "accounting-service"
#
#resource "openstack_compute_servergroup_v2" "ollama_dev_server_group" {
#  name     = "ollama-dev"
#  policies = [module.common_variables.policy_list.soft_anti_affinity]
#}

#module "instance_without_data_volume" {
#  source = "../../modules/instance_without_data_volume"

  #instances_without_data_volume_map = {
  #  test_migrazione840_1 = {
  ##    name              = "test-migrazione840-1",
    #  description       = "This instance serves as testing image for 840 migration",
   #   flavor            = module.common_variables.flavor_list.m1_medium,
   #   networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.timescaledb],
   #   security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
   #   server_groups_ids = [openstack_compute_servergroup_v2.test_migratione840_1_server_group.id],
   #   image_ref         = module.common_variables.ubuntu_1804
   # }
  #}
#}
