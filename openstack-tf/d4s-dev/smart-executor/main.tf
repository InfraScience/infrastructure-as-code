# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}


#
# Creates the server group "accounting-dashboard-harvester-se-plugin"
#
resource "openstack_compute_servergroup_v2" "accounting_dashboard_harvester_server_group" {
  name     = "accounting-dashboard-harvester-se-plugin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "accounting-aggregator-se-plugin"
#
resource "openstack_compute_servergroup_v2" "accounting_aggregator_server_group" {
  name     = "accounting-aggregator-se-plugin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "resource-checker-se-plugin"
#
resource "openstack_compute_servergroup_v2" "resource_checker_server_group" {
  name     = "resource-checker-se-plugin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "social-data-indexer-se-plugin"
#
resource "openstack_compute_servergroup_v2" "social_data_indexer_server_group" {
  name     = "social-data-indexer-se-plugin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}

#
# Creates the server group "accounting-insert-storage-se-plugin"
#
resource "openstack_compute_servergroup_v2" "accounting_insert_storage_server_group" {
  name     = "accounting-insert-storage-se-plugin"
  policies = [module.common_variables.policy_list.soft_anti_affinity]
}


module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    accounting_dashboard_harvester_se_plugin = {
      name              = "accounting-dashboard-harvester-se-plugin",
      description       = "This instance serves smart-executor service with accounting-dashboard-harvester-se-plugin",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.orientdb_se, module.common_variables.networks_list.timescaledb, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [openstack_compute_servergroup_v2.accounting_dashboard_harvester_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    accounting_aggregator_se_plugin = {
      name              = "accounting-aggregator-se-plugin",
      description       = "This instance serves smart-executor service with accounting-aggregator-se-plugin",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.orientdb_se, module.common_variables.networks_list.timescaledb],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [openstack_compute_servergroup_v2.accounting_aggregator_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    resource_checker_se_plugin = {
      name              = "resource-checker-se-plugin",
      description       = "This instance serves smart-executor service with resource-checker-se-plugin",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.orientdb_se],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [openstack_compute_servergroup_v2.resource_checker_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    # Social Data Indexer should access to Cassandra too
    social_data_indexer_se_plugin = {
      name              = "social-data-indexer-se-plugin",
      description       = "This instance serves smart-executor service with social-data-indexer-se-plugin",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.orientdb_se],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [openstack_compute_servergroup_v2.social_data_indexer_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    },
    accounting_insert_storage_se_plugin = {
      name              = "accounting-insert-storage-se-plugin",
      description       = "This instance serves smart-executor service with accounting-insert-storage-se-plugin",
      flavor            = module.common_variables.flavor_list.m1_medium,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.orientdb_se],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [openstack_compute_servergroup_v2.accounting_insert_storage_server_group.id],
      image_ref         = module.common_variables.ubuntu_1804
    }
  }
}
