# NFS shares required by the CCP
# Create a NFS share for the repository data
#
resource "openstack_sharedfilesystem_share_v2" "ccp_dev_repository_data" {
  name        = "ccp_dev_repository_data"
  description = "NFS share for the CCP repository data"
  share_proto = "NFS"
  size        = 5
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "ccp_dev_repository_nfs_access" {
  for_each     = var.swarm_manila_interfaces_ip
  share_id     = openstack_sharedfilesystem_share_v2.ccp_dev_repository_data.id
  access_type  = "ip"
  access_to    = each.value
  access_level = "rw"
}

# NFS shares required by the CCP
# Create a NFS share for the method logs
#
resource "openstack_sharedfilesystem_share_v2" "ccp_dev_methods_logs" {
  name        = "ccp_dev_method_logs"
  description = "NFS share for the CCP method logs"
  share_proto = "NFS"
  size        = 1
}

# Allow access to the NFS share
resource "openstack_sharedfilesystem_share_access_v2" "ccp_dev_methods_logs_nfs_access" {
  for_each     = var.swarm_manila_interfaces_ip
  share_id     = openstack_sharedfilesystem_share_v2.ccp_dev_methods_logs.id
  access_type  = "ip"
  access_to    = each.value
  access_level = "rw"
}

output "ccp_dev_repository_data" {
  value = openstack_sharedfilesystem_share_v2.ccp_dev_repository_data
}

output "ccp_dev_repository_data_nfs_acls" {
  value = openstack_sharedfilesystem_share_access_v2.ccp_dev_repository_nfs_access
  sensitive = true
}

output "ccp_dev_methods_logs" {
  value = openstack_sharedfilesystem_share_v2.ccp_dev_methods_logs
}

output "ccp_dev_methods_logs_access_nfs_acls" {
  value = openstack_sharedfilesystem_share_access_v2.ccp_dev_methods_logs_nfs_access
  sensitive = true
}
