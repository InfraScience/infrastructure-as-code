# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

locals {
  cname_target = "swarm-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# We only manage the DNS records, for the services behind HAProxy
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {

    vremodeler-dev = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["vremodeler", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Vremodeler dev"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }

    netme-sobigdata = {
      name        = join(".", ["netme-sobigdata", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      records     = [local.cname_target]
      type        = "CNAME"
      description = "SoBigData NetMe service"
      ttl         = 8600
    }
    
    ontotagme-sobigdata = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["ontotagme-sobigdata", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "SoBigData OntoTagme service"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }

    shinyproxy-dev = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["shinyproxy", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Shinyproxy DEV instance"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }

  }
}
