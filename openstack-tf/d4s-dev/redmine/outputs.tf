output "nfs_port_data" {
  value = openstack_compute_interface_attach_v2.nfs_port_to_redmine
}

output "redmine_nfs_volume_data" {
  value = openstack_sharedfilesystem_share_v2.redmine_dev
}
