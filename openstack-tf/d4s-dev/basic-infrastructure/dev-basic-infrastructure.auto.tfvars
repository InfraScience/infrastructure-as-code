shared_postgresql_server_data = {
  name ="postgresql-server"
  flavor = "m1.large"
  vol_data_name = "postgresql-server-data"
  vol_data_size = "200"
  vol_data_device = "/dev/vdb"
  network_name = "postgresql-srv-net"
  network_description = "Network used to communicate with the shared postgresql service"
  network_cidr = "192.168.2.0/24"
  allocation_pool_start = "192.168.2.10"
  allocation_pool_end = "192.168.3.254"
  server_ip = "192.168.2.153"
  server_cidr = "192.168.2.153/22"
}

# Provided in the output of the project setup
main_private_network_id = "e0af5eba-f24a-4d0d-8184-bc654b980c4a"
main_private_subnet_id = "2aa977f2-80b4-447c-a6b0-dfa06bf68751"
dns_zone_id  = "cbae638a-9d99-44aa-946c-0f5ffb7fc488"

octavia_information = {
    main_lb_name = "lb-dev-l4"
    main_lb_description = "Main L4 load balancer for the D4Science DEV"
    octavia_flavor = "octavia_amphora-mvcpu-ha"
    octavia_flavor_id = "394988b5-6603-4a1e-a939-8e177c6681c7"
    main_lb_hostname = "main-lb"
    # The following aren't available when the module runs so we have to get them with the command
    # openstack --os-cloud d4s-pre port list -f value | grep octavia-lb-vrrp
    # This means that the execution will fail
    octavia_vrrp_ip_1 = "10.1.29.161/32"
    octavia_vrrp_ip_2 = "10.1.30.180/32"
}

