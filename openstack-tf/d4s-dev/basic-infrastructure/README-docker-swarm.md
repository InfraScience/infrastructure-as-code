# Docker Swarm

Docker Swarm cluster behind a Octavia balancer L4

* One Octavia Load balancer
* 3 VMs for the master nodes with their server group (soft anti affinity)
* 7 VMs for the worker nodes with their server group (soft anti affinity)
* 1 VM for the NFS service
* 1 dedicated network, for the traffic of the NFS exports
