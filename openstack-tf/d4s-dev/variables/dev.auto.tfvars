#
os_project_data = {
    id = "e8f8ca72f30648a8b389b4e745ac83a9"
}

dns_zone = {
    zone_name   = "cloud-dev.d4science.org."
    email       = "postmaster@isti.cnr.it"
    description = "DNS primary zone for the d4s-dev-cloud project"
    ttl         = 8600
}

default_security_group_name = "default"

main_private_network = {
    name        = "d4s-dev-cloud-main"
    description = "D4Science DEV private network (use this as the main network)"
}

main_private_subnet = {
    name             = "d4s-dev-cloud-sub"
    description      = "D4Science DEV main private subnet"
    cidr             = "10.1.28.0/22"
    gateway_ip       = "10.1.28.1"
    allocation_start = "10.1.28.30"
    allocation_end   = "10.1.31.254"
}

external_router = {
    name        = "d4s-dev-cloud-external-router"
    description = "D4Science DEV main router"
    id          = "2ae28c5f-036b-45db-bc9f-5bab8fa3e914"
}

basic_services_ip = {
    ca                = "10.1.29.247"
    ca_cidr           = "10.1.29.247/32"
    ssh_jump          = "10.1.29.164"
    ssh_jump_cidr     = "10.1.29.164/32"
    prometheus        = "10.1.30.129"
    prometheus_cidr   = "10.1.30.129/32"
    haproxy_l7_1      = "10.1.28.50"
    haproxy_l7_1_cidr = "10.1.28.50/32"
    haproxy_l7_2      = "10.1.30.241"
    haproxy_l7_2_cidr = "10.1.30.241/32"
    octavia_main      = "10.1.28.227"
    octavia_main_cidr = "10.1.28.227/32"
}

main_haproxy_l7_ip = ["10.1.28.50", "10.1.30.241"]

octavia_information = {
    main_lb_name        = "lb-dev-l4"
    main_lb_description = "Main L4 load balancer for the D4Science DEV"
    octavia_flavor      = "octavia_amphora-mvcpu-ha"
    octavia_flavor_id   = "394988b5-6603-4a1e-a939-8e177c6681c7"
    main_lb_hostname    = "main-lb"
}
