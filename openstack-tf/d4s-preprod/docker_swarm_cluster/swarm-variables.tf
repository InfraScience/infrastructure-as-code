variable "docker_swarm_data" {
  type = map(string)
  default = {
    mgr_name           = "swarm-mgr"
    mgr1_ip            = "10.1.32.31"
    mgr1_cidr          = "10.1.32.31/32"
    mgr2_ip            = "10.1.32.32"
    mgr2_cidr          = "10.1.32.32/32"
    mgr3_ip            = "10.1.32.33"
    mgr3_cidr          = "10.1.32.33/32"
    mgr_count          = 3
    mgr_flavor         = "m1.large"
    mgr_data_disk_size = 100
  }
}

variable "swarm_managers_ip" {
  type    = list(string)
  default = ["10.1.32.31", "10.1.32.32", "10.1.32.33"]

}

variable "octavia_swarm_data" {
  type = map(string)
  default = {
    swarm_lb_name           = "d4s-pre-cloud-swarm-l4"
    swarm_lb_description    = "L4 balancer that serves the D4Science pre Docker Swarm cluster"
    swarm_lb_name           = "d4s-pre-cloud-swarm-l4"
    swarm_lb_hostname       = "swarm-lb"
    swarm_octavia_main_ip   = "10.1.32.30"
    swarm_octavia_main_cidr = "10.1.32.30/32"
  }
}
