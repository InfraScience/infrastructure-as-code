# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

#Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}


resource "openstack_networking_secgroup_v2" "dataminer_publish" {
  name        = "dataminer_publish"
  description = "Access to dataminer-ghost is allowed only to dm-pool-manager"
  delete_default_rules = "true"
}


resource "openstack_compute_instance_v2" "dm_pool_manager_pre" {
  name   = "dm-pool-manager-pre"
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = module.common_variables.flavor_list.m1_large
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = module.common_variables.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 30
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = toset([data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name])
    content {
      name = network.value
    }
  }

  
  # user_data script used
  user_data = file("${module.common_variables.ubuntu_1804.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_1" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = join("/",[openstack_compute_instance_v2.dm_pool_manager_pre.network.0.fixed_ip_v4,"32"])
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_2" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_dataminer_publish_rule_3" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = join("/",[openstack_compute_instance_v2.dm_pool_manager_pre.network.0.fixed_ip_v4,"32"])
  security_group_id = openstack_networking_secgroup_v2.dataminer_publish.id
}



module "instance_without_data_volume" {
  source = "../../modules/instance_without_data_volume"

  instances_without_data_volume_map = {
    dataminer_pre_ghost = {
      name              = "dataminer-pre-ghost",
      description       = "This instance is a DataMiner Ghost service",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [openstack_networking_secgroup_v2.dataminer_publish.name,data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 50
    },
    dataminer_1_pre_1804 = {
      name              = "dataminer1-pre-1804",
      description       = "This instance is a DataMiner service",
      flavor            = module.common_variables.flavor_list.m1_large,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name, module.common_variables.networks_list.shared_postgresql],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804
      image_volume_size = 50
    }
  }
}

locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    dm-pool-manager-pre = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dm-pool-manager-pre", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DM Pool Manager Preproduction"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer-pre-ghost = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer-pre-ghost", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner Preproduction Ghost"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    },
    dataminer1-pre-1804 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["dataminer1-pre-1804", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "DataMiner 1 Pre 1804"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
