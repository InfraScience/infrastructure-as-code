# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}

module "keycloak" {
  source = "../../modules/keycloak"

  keycloak_data = {
    affinity_policy   = "soft-anti-affinity"
    srv_name          = "keycloak-pre"
    vm_count          = 2
    vm_flavor         = "m1.medium"
    boot_vol_size     = 10
    share_description = "NFS share for the keycloak preprod static data"
    share_name        = "keycloak_pre_nfs_share"
  }

  keycloak_recordsets = {
    keycloak_main_record = {
      name        = join(".", ["accounts", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name]),
      description = "Keycloak preproduction endpoint"
    }
  }

  keycloak_object_store = "keycloak-data-preprod"
}
