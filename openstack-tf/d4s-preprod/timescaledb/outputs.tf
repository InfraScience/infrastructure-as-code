output "timescaledb_net" {
  value = module.timescaledb.timescaledb_net
}

output "timescaledb_subnet" {
  value = module.timescaledb.timescaledb_subnet
}

output "timescaledb_servers" {
  value = module.timescaledb.timescaledb_servers
  sensitive = true
}
