# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

module "common_variables" {
  source = "../../modules/common_variables"
}

module "orientdb" {
  source = "../../modules/orientdb"

  orientdb_nodes_count    = 1
  orientdb_node_flavor    = "m1.medium"
  orientdb_image_name = module.common_variables.ubuntu_2204.name
  orientdb_image_uuid = module.common_variables.ubuntu_2204.uuid
  orientdb_se_node_flavor = "m1.medium"
  orientdb_se_image_name = module.common_variables.ubuntu_2204.name
  orientdb_se_image_uuid = module.common_variables.ubuntu_2204.uuid
  orientdb_se_ip          = "192.168.12.4"
  orientdb_se_cidr        = "192.168.12.4/32"
}
