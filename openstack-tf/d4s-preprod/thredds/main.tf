# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

#
# Uses common_variables as module
#
module "common_variables" {
  source = "../../modules/common_variables"
}


module "instance_with_data_volume" {
  source = "../../modules/instance_with_data_volume"

  instances_with_data_volume_map = {
    thredds1 = {
      name              = "thredds1",
      description       = "Thredds pre instance",
      flavor            = module.common_variables.flavor_list.m2_small,
      networks          = [data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name],
      security_groups   = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, data.terraform_remote_state.privnet_dns_router.outputs.security_group_list.http_and_https_from_the_load_balancers],
      server_groups_ids = [],
      image_ref         = module.common_variables.ubuntu_1804,
      image_volume_size = 20
      volume = {
        name   = "thredds1_data_volume",
        size   = "280",
        device = "/dev/vdb"
      }
    }
  }
}


locals {
  cname_target = "main-lb.${data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name}"
}

#
# Add DNS record/s
#
module "dns_records_create" {
  source = "../../modules/dns_resources"

  dns_resources_map = {
    thredds1 = {
      zone_id     = data.terraform_remote_state.privnet_dns_router.outputs.dns_zone_id
      name        = join(".", ["thredds1", data.terraform_remote_state.privnet_dns_router.outputs.dns_zone.zone_name])
      description = "Thredds thredds1"
      ttl         = 8600
      type        = "CNAME"
      records     = [local.cname_target]
    }
  }
}
