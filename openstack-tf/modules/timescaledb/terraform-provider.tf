# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Global variables (constants, really)
module "common_variables" {
  source = "../../modules/common_variables"
}

