module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

module "common_variables" {
  source = "../../modules/common_variables"
}

# Geo_Service attached volume - used for 'external volume'
resource "openstack_blockstorage_volume_v3" "geo_service_data_volume" {
  for_each = var.geo_service_instances_map
  name     = each.value.vol_data_name
  size     = each.value.vol_data_size
}

# Geo_Service instance
resource "openstack_compute_instance_v2" "geo_service" {
  for_each                = var.geo_service_instances_map
  name                    = each.value.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = each.value.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = each.value.security_groups
  block_device {
    uuid                  = each.value.image_ref.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = each.value.networks
    content {
      name = network.value
    }
  }

  # Creates the scheduler_hints (i.e. server groups) according to input server_groups_ids
  dynamic "scheduler_hints" {
    for_each = each.value.server_groups_ids
    content {
      group = scheduler_hints.value
    }
  }
  user_data = file("${each.value.image_ref.user_data_file}")
}


# Attach the additional volume
resource "openstack_compute_volume_attach_v2" "geo_service_data_attach_vol" {
  for_each    = var.geo_service_instances_map
  instance_id = openstack_compute_instance_v2.geo_service[each.key].id
  volume_id   = openstack_blockstorage_volume_v3.geo_service_data_volume[each.key].id
  device      = var.geo_instance_basic_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.geo_service]
}
