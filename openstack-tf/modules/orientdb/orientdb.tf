# OrientDB and OrientDB for the smart executors
#
resource "openstack_compute_servergroup_v2" "orientdb_cluster" {
  name     = "orientdb_cluster"
  policies = [var.orientdb_affinity_policy]
}
#
# Network for the cluster traffic
#
resource "openstack_networking_network_v2" "orientdb_network" {
  name                  = var.orientdb_net.network_name
  admin_state_up        = "true"
  external              = "false"
  description           = var.orientdb_net.network_description
  mtu                   = module.common_variables.mtu_size
  port_security_enabled = true
  shared                = false
  region                = module.common_variables.main_region
}

# Subnet
resource "openstack_networking_subnet_v2" "orientdb_subnet" {
  name            = "orientdb-subnet"
  description     = "Subnet used by the OrientDB service"
  network_id      = openstack_networking_network_v2.orientdb_network.id
  cidr            = var.orientdb_net.network_cidr
  dns_nameservers = module.common_variables.resolvers_ip
  ip_version      = 4
  enable_dhcp     = true
  no_gateway      = true
  allocation_pool {
    start = var.orientdb_net.allocation_pool_start
    end   = var.orientdb_net.allocation_pool_end
  }
}

#
# Network for the OrientDB SE
#
resource "openstack_networking_network_v2" "orientdb_se_network" {
  name                  = var.orientdb_se_net.network_name
  admin_state_up        = "true"
  external              = "false"
  description           = var.orientdb_se_net.network_description
  mtu                   = module.common_variables.mtu_size
  port_security_enabled = true
  shared                = false
  region                = module.common_variables.main_region
}

# Subnet
resource "openstack_networking_subnet_v2" "orientdb_se_subnet" {
  name            = "orientdb-se-subnet"
  description     = "Subnet used by the OrientDB for Smart Executor"
  network_id      = openstack_networking_network_v2.orientdb_se_network.id
  cidr            = var.orientdb_se_net.network_cidr
  dns_nameservers = module.common_variables.resolvers_ip
  ip_version      = 4
  enable_dhcp     = true
  no_gateway      = true
  allocation_pool {
    start = var.orientdb_se_net.allocation_pool_start
    end   = var.orientdb_se_net.allocation_pool_end
  }
}

#
# Security groups
#
# Main OrientDB service
# Between OrientDB nodes
resource "openstack_networking_secgroup_v2" "orientdb_internal_traffic" {
  name                 = "orientdb_internal_traffic"
  delete_default_rules = "true"
  description          = "Traffic between the OrientDB nodes"
}
resource "openstack_networking_secgroup_rule_v2" "orientdb_ports" {
  count             = var.orientdb_nodes_count
  security_group_id = openstack_networking_secgroup_v2.orientdb_internal_traffic.id
  description       = "TCP traffic between OrientDB nodes"
  port_range_min    = 2424
  port_range_max    = 2490
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  # remote_ip_prefix = format("%s-%02d", var.orientdb_ip, count.index+1, "/32")
  remote_ip_prefix = var.orientdb_cidr.* [count.index]
}
# Access from the clients
resource "openstack_networking_secgroup_v2" "access_to_orientdb" {
  name                 = "access_to_orientdb"
  delete_default_rules = "true"
  description          = "Clients that talk to the OrientDB service"
}
resource "openstack_networking_secgroup_rule_v2" "access_to_orient_from_clients" {
  for_each          = toset([data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.ssh_jump_cidr, openstack_networking_subnet_v2.orientdb_subnet.cidr])
  security_group_id = openstack_networking_secgroup_v2.access_to_orientdb.id
  description       = "TCP traffic from the resource registries and the SSH jump server"
  port_range_min    = 2424
  port_range_max    = 2490
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = each.value
}
resource "openstack_networking_secgroup_rule_v2" "access_to_orient_from_haproxy" {
  for_each          = toset([data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.haproxy_l7_1_cidr, data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.haproxy_l7_2_cidr, data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr])
  security_group_id = openstack_networking_secgroup_v2.access_to_orientdb.id
  description       = "TCP traffic from the load balancers"
  port_range_min    = 2480
  port_range_max    = 2480
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = each.value
}

# OrientDB for the Smart Executor nodes
# Access from the clients
resource "openstack_networking_secgroup_v2" "access_to_orientdb_se" {
  name                 = "access_to_orientdb_se"
  delete_default_rules = "true"
  description          = "Clients that talk to the OrientDB SE service"
}
resource "openstack_networking_secgroup_rule_v2" "access_to_orient_se_from_clients" {
  for_each          = toset([data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.ssh_jump_cidr, openstack_networking_subnet_v2.orientdb_se_subnet.cidr])
  security_group_id = openstack_networking_secgroup_v2.access_to_orientdb_se.id
  description       = "TCP traffic from the smart executors and the SSH jump server"
  port_range_min    = 2424
  port_range_max    = 2490
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = each.value
}
resource "openstack_networking_secgroup_rule_v2" "access_to_orient_se_from_haproxy" {
  for_each          = toset([data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.haproxy_l7_1_cidr, data.terraform_remote_state.privnet_dns_router.outputs.basic_services_ip.haproxy_l7_2_cidr, data.terraform_remote_state.privnet_dns_router.outputs.main_private_subnet.cidr])
  security_group_id = openstack_networking_secgroup_v2.access_to_orientdb_se.id
  description       = "TCP traffic from the load balancers"
  port_range_min    = 2480
  port_range_max    = 2480
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = each.value
}

#
# OrientDB main cluster
#
# Instances used by the resource registry
resource "openstack_compute_instance_v2" "orientdb_servers" {
  count                   = var.orientdb_nodes_count
  name                    = format("%s-%02d", var.orientdb_data.node_name, count.index + 1)
  availability_zone_hints = module.common_variables.availability_zones_names.availability_zone_no_gpu
  image_name              = var.orientdb_image_name
  flavor_name             = var.orientdb_node_flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.orientdb_internal_traffic.name, openstack_networking_secgroup_v2.access_to_orientdb.name]
  scheduler_hints {
    group = openstack_compute_servergroup_v2.orientdb_cluster.id
  }
  block_device {
    uuid                  = var.orientdb_image_uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.orientdb_data.node_data_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }
  network {
    name        = var.orientdb_net.network_name
    fixed_ip_v4 = var.orientdb_ip.* [count.index]
  }

  user_data  = file("${module.common_variables.ubuntu_2204.user_data_file}")
  depends_on = [openstack_networking_subnet_v2.orientdb_subnet]
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

# Instance used by the smart executors
resource "openstack_compute_instance_v2" "orientdb_se_server" {
  name                    = "orientdb-se"
  availability_zone_hints = module.common_variables.availability_zones_names.availability_zone_no_gpu
  image_name              = var.orientdb_se_image_name
  flavor_name             = var.orientdb_se_node_flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [data.terraform_remote_state.privnet_dns_router.outputs.default_security_group_name, openstack_networking_secgroup_v2.access_to_orientdb_se.name]
  block_device {
    uuid                  = var.orientdb_image_uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  block_device {
    source_type           = "blank"
    volume_size           = var.orientdb_data.node_data_disk_size
    boot_index            = -1
    destination_type      = "volume"
    delete_on_termination = false
  }

  network {
    name = data.terraform_remote_state.privnet_dns_router.outputs.main_private_network.name
  }
  network {
    name        = var.orientdb_se_net.network_name
    fixed_ip_v4 = var.orientdb_se_ip
  }

  user_data  = file("${module.common_variables.ubuntu_2204.user_data_file}")
  depends_on = [openstack_networking_subnet_v2.orientdb_se_subnet]
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}

