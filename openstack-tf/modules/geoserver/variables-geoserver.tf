#Not used
variable "geoserver_basic" {
  type = map(string)
  default = {
    name        = "geoserver"
    description = "Geoserver instance"
    flavor      = "m1.medium"
  }
}

#Default geoserver vol_data_device
variable "geoserver_basic_data" {
  type = map(string)
  default = {
    vol_data_device = "/dev/vdb"
  }
}

#Default geoserver_instances_map is EMPTY. Override it to create a proper geoserver plan
variable "geoserver_instances_map" {
  type = map(object({
    name          = string
    description   = string
    flavor        = string
    vol_data_name = string
    vol_data_size = string
  }))
  default = {
    geoserver = { name = "", description = "", flavor = "", vol_data_name = "", vol_data_size = "" }
  }

}
