# Creates single geoserver instance

# # Geoserver attached volume - used for 'geoserver_data'
# resource "openstack_blockstorage_volume_v3" "geoserver_data_vol" {
#   name = var.geoserver_basic_data.vol_data_name
#   size = var.geoserver_basic_data.vol_data_size
# }

# # Geoserver instance
# resource "openstack_compute_instance_v2" "geoserver" {
#   name                    = var.geoserver_basic.name
#   availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
#   flavor_name             = var.geoserver_basic.flavor
#   key_pair                = module.ssh_settings.ssh_key_name
#   security_groups         = [var.security_group_list.default, var.security_group_list.http_and_https_from_the_load_balancers]
#   block_device {
#     uuid                  = var.ubuntu_1804.uuid
#     source_type           = "image"
#     volume_size           = 10
#     boot_index            = 0
#     destination_type      = "volume"
#     delete_on_termination = false
#   }
#   network {
#     name = var.main_private_network.name
#   }
#   network {
#     name = var.shared_postgresql_server_data.network_name
#   }
#   user_data = file("${var.ubuntu1804_data_file}")
# }


# # Attach the additional volume
# resource "openstack_compute_volume_attach_v2" "geoserver_data_attach_vol" {
#   instance_id = openstack_compute_instance_v2.geoserver.id
#   volume_id   = openstack_blockstorage_volume_v3.geoserver_data_vol.id
#   device      = var.geoserver_basic_data.vol_data_device
#   depends_on  = [openstack_compute_instance_v2.geoserver]
# }


# Creates all the geoserver instances with volumes declared in the var.geoserver_instances_map 

# variable "geoserver_instances" {
#   type = map(object({
#     name          = string
#     description   = string
#     flavor        = string
#     vol_data_name = string
#     vol_data_size = string
#   }))
#   default = {
#     geoserver = { name = "", description = "", flavor = "", vol_data_name = "", vol_data_size = "" }
#   }

# }

# Geoserver attached volume - used for 'geoserver_data'
resource "openstack_blockstorage_volume_v3" "geoserver_data_volume" {
  for_each = var.geoserver_instances_map
  name     = each.value.vol_data_name
  size     = each.value.vol_data_size
}

# Geoserver instance
resource "openstack_compute_instance_v2" "geoserver" {
  for_each                = var.geoserver_instances_map
  name                    = each.value.name
  availability_zone_hints = var.availability_zones_names.availability_zone_no_gpu
  flavor_name             = each.value.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = [var.security_group_list.default, var.security_group_list.http_and_https_from_the_load_balancers]
  block_device {
    uuid                  = var.ubuntu_1804.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }
  network {
    name = var.main_private_network.name
  }
  network {
    name = var.shared_postgresql_server_data.network_name
  }
  user_data = file("${var.ubuntu1804_data_file}")
}


# Attach the additional volume
resource "openstack_compute_volume_attach_v2" "geoserver_data_attach_vol" {
  for_each    = var.geoserver_instances_map
  instance_id = openstack_compute_instance_v2.geoserver[each.key].id
  volume_id   = openstack_blockstorage_volume_v3.geoserver_data_volume[each.key].id
  device      = var.geoserver_basic_data.vol_data_device
  depends_on  = [openstack_compute_instance_v2.geoserver]
}
