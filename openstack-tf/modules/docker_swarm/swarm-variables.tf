variable "docker_swarm_data" {
  type = map(string)
  default = {
    mgr_name                    = "swarm-mgr"
    mgr1_ip                     = "10.1.40.31"
    mgr1_cidr                   = "10.1.40.31/32"
    mgr2_ip                     = "10.1.40.32"
    mgr2_cidr                   = "10.1.40.32/32"
    mgr3_ip                     = "10.1.40.33"
    mgr3_cidr                   = "10.1.40.33/32"
    mgr_count                   = 3
    mgr_flavor                  = "m1.large"
    mgr_data_disk_size          = 200
    worker_name                 = "swarm-worker"
    worker_count                = 5
    worker_flavor               = "m1.xlarge"
    worker_data_disk_size       = 200
    nfs_server_name             = "swarm-nfs-server"
    nfs_server_flavor           = "m1.medium"
    nfs_server_data_disk_name   = "Swarm NFS server data Disk"
    nfs_server_data_disk_size   = 100
    nfs_server_data_disk_device = "/dev/vdb"
  }
}

variable "swarm_managers_ip" {
  type    = list(string)
  default = ["10.1.40.31", "10.1.40.32", "10.1.40.33"]

}

variable "octavia_swarm_data" {
  type = map(string)
  default = {
    swarm_lb_name           = "d4s-production-cloud-swarm-l4"
    swarm_lb_description    = "L4 balancer that serves the D4Science production Docker Swarm cluster"
    swarm_lb_name           = "d4s-production-cloud-swarm-l4"
    octavia_flavor          = "octavia_amphora-mvcpu-ha"
    octavia_flavor_id       = "394988b5-6603-4a1e-a939-8e177c6681c7"
    swarm_lb_hostname       = "swarm-lb"
    swarm_octavia_main_ip   = "10.1.40.30"
    swarm_octavia_main_cidr = "10.1.40.30/32"
  }
}

variable "swarm_nfs_private_network" {
  type = map(string)
  default = {
    network_name          = "swarm-nfs-net"
    network_description   = "Network used by the swarm nodes and the NFS service"
    network_cidr          = "192.168.4.0/23"
    allocation_pool_start = "192.168.4.100"
    allocation_pool_end   = "192.168.5.254"
    server_ip             = "192.168.4.10"
    server_cidr           = "192.168.4.5/23"
  }
}
