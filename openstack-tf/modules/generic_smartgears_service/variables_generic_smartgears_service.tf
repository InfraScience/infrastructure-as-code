
#Default smartgears_service is EMPTY. Override it to create a proper smartegears plan
variable "smartgears_service_instances_map" {
  type = map(object({
    name              = string
    description       = string
    flavor            = string
    networks          = list(string)
    security_groups   = list(string)
    server_groups_ids = list(string)
    image_ref         = map(string)
  }))
  default = {
    smartgears_service = { name = "", description = "", flavor = "", networks = [], security_groups = [], server_groups_ids = [], image_ref = {} }
  }

}
