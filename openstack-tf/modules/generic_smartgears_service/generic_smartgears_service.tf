
# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Module used
module "common_variables" {
  source = "../../modules/common_variables"
}

# Generic smartgears_service instance
resource "openstack_compute_instance_v2" "smartgears_service" {
  for_each                = var.smartgears_service_instances_map
  name                    = each.value.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = each.value.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = each.value.security_groups
  block_device {
    uuid                  = each.value.image_ref.uuid
    source_type           = "image"
    volume_size           = 10
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = each.value.networks
    content {
      name = network.value
    }
  }

  # Creates the scheduler_hints (i.e. server groups) according to input server_groups_ids
  dynamic "scheduler_hints" {
    for_each = each.value.server_groups_ids
    content {
      group = scheduler_hints.value
    }
  }

  # user_data script used
  user_data = file("${each.value.image_ref.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }

}
