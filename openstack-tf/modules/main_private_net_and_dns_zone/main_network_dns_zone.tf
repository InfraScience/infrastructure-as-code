resource "openstack_dns_zone_v2" "primary_project_dns_zone" {
  name = var.dns_zone.zone_name
  email       = var.dns_zone.email
  description = var.dns_zone.description
  project_id = var.os_project_data.id
  ttl         = var.dns_zone.ttl
  type        = "PRIMARY"
}

resource "openstack_networking_network_v2" "main-private-network" {
  name           = var.main_private_network.name
  admin_state_up = "true"
  external       = "false"
  description = var.main_private_network.description
  dns_domain = var.dns_zone.zone_name
  mtu            = module.common_variables.mtu_size
  port_security_enabled = true
  shared = false
  region = module.common_variables.main_region
  tenant_id = var.os_project_data.id
}

resource "openstack_networking_subnet_v2" "main-private-subnet" {
  name            = var.main_private_subnet.name
  description = var.main_private_subnet.description
  network_id      = openstack_networking_network_v2.main-private-network.id
  cidr            = var.main_private_subnet.cidr
  gateway_ip      = var.main_private_subnet.gateway_ip
  dns_nameservers = module.common_variables.resolvers_ip
  ip_version = 4
  enable_dhcp = true
  tenant_id = var.os_project_data.id
  allocation_pool {
    start = var.main_private_subnet.allocation_start
    end   = var.main_private_subnet.allocation_end
  }
}

resource "openstack_networking_router_v2" "external-router" {
  name                = var.external_router.name
  description = var.external_router.description
  external_network_id = module.common_variables.external_network.id
  tenant_id = var.os_project_data.id
  enable_snat = true
   vendor_options {
    set_router_gateway_after_create = true
  }
}

# Router interface configuration
resource "openstack_networking_router_interface_v2" "private-network-routing" {
  router_id = openstack_networking_router_v2.external-router.id
  # router_id = var.external_router.id
  subnet_id = openstack_networking_subnet_v2.main-private-subnet.id
}

locals {
  acme_challenge_recordset_name = "_acme-challenge.${var.dns_zone.zone_name}"
  acme_challenge_delegation = "_acme-challenge.d4science.net."
}

resource "openstack_dns_recordset_v2" "acme_challenge_recordset" {
  zone_id     = openstack_dns_zone_v2.primary_project_dns_zone.id
  name        = local.acme_challenge_recordset_name
  description = "ACME challenge delegation"
  ttl         = 8600
  type        = "CNAME"
  records     = ["_acme-challenge.d4science.net."]
}

resource "openstack_networking_secgroup_v2" "nfs_share_no_ingress" {
  name                 = "nfs_share_no_ingress"
  delete_default_rules = "true"
  description          = "Security rule that must be assigned to the NFS ports"
}

resource "openstack_networking_secgroup_rule_v2" "egress_ipv4_allowed" {
  security_group_id = openstack_networking_secgroup_v2.nfs_share_no_ingress.id
  description       = "Allow the egress traffic from the NFS port"
  direction         = "egress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
}
