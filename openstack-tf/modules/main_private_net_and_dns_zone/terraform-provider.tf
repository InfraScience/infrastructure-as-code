# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.53.0"
    }
  }
}

# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Module used
module "common_variables" {
  source = "../../modules/common_variables"
}

