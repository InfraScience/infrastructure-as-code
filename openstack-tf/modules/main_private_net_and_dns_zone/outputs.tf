output "dns_zone_id" {
  value = openstack_dns_zone_v2.primary_project_dns_zone.id
}

output "main_private_network_id" {
  value = openstack_networking_network_v2.main-private-network.id
}

output "main_subnet_network_id" {
  value = openstack_networking_subnet_v2.main-private-subnet.id
}

output "external_gateway_ip" {
  value = openstack_networking_router_v2.external-router.external_fixed_ip
}

output "storage_nfs_network_id" {
  value = var.storage_nfs_network_id
}

output "storage_nfs_subnet_id" {
  value = var.storage_nfs_subnet_id
}

output "nfs_share_no_ingress_secgroup_id" {
  value = openstack_networking_secgroup_v2.nfs_share_no_ingress.id
}

