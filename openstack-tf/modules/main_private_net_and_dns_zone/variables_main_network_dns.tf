variable "os_project_data" {
  type = map(string)
  default = {
    id = ""
  }
}

variable "dns_zone" {
  type = map(string)
  default = {
    zone_name   = ""
    email       = "postmaster@isti.cnr.it"
    description = ""
    ttl         = 8600
    id          = ""
  }
}

variable "main_private_network" {
  type = map(string)
  default = {
    name        = ""
    description = ""
  }
}

variable "main_private_subnet" {
  type = map(string)
  default = {
    name             = ""
    description      = ""
    cidr             = ""
    gateway_ip       = ""
    allocation_start = ""
    allocation_end   = ""
  }
}

variable "external_router" {
  type = map(string)
  default = {
    name        = ""
    description = ""
  }
}

variable "storage_nfs_network_id" {
  default = "5f4023cc-4016-404c-94e5-86220095fbaf"
}

variable "storage_nfs_subnet_id" {
  default = "6ff0f9e8-0e74-4cc3-a268-7ed4af435696"
}

