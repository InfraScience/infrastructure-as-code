variable "liferay_data" {
  type = map(string)
  default = {
    affinity_policy = "soft-anti-affinity"
    srv_name        = "lr62"
    vm_count        = 1
    vm_flavor       = "m1.large"
    boot_vol_size   = 30
    share_description = "NFS share for the liferay static data"
    share_name = "liferay_nfs_share"
  }
}

variable "liferay_ip_addrs" {
  type    = list(string)
  default = []
}

variable "liferay_recordsets" {
  type    = map(object({
    name = string
    description = string
  }))
  default = {
    liferay_dns_record = {
      name = "",
      description = ""
    }
  }
}

