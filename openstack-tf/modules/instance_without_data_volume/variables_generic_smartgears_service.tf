
# Default instances without data volume is EMPTY. Override it to create a proper instance plan
variable "instances_without_data_volume_map" {
  type = map(object({
    name              = string
    description       = string
    flavor            = string
    networks          = list(string)
    security_groups   = list(string)
    server_groups_ids = list(string)
    image_ref         = map(string)
    image_volume_size = optional(number, 10)
  }))
  default = {
    smartgears_service = {
      name              = "",
      description       = "",
      flavor            = "",
      networks          = [],
      security_groups   = [],
      server_groups_ids = [],
      image_ref         = {}
    }
  }

}
