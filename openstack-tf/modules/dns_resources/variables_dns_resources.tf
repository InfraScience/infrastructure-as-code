
#Default DNS resource is EMPTY. Override it to create a proper plan
variable "dns_resources_map" {
  type = map(object({
    zone_id     = string
    name        = string
    description = string
    ttl         = number
    type        = string
    records     = list(string)
  }))
  default = {
    dns_record = {
      zone_id     = ""
      name        = "",
      description = "",
      ttl         = 8600,
      type        = "CNAME",
      records     = []
    }
  }

}
