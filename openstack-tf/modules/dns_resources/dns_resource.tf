
# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_dns_recordset_v2" "add_dns_recordset" {
  for_each    = var.dns_resources_map
  zone_id     = each.value.zone_id
  name        = each.value.name
  description = each.value.description
  ttl         = each.value.ttl
  type        = each.value.type
  records     = each.value.records
}
