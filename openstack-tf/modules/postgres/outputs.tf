
output "postgres_instance_data" {
  value = var.postgres_instance_data
}

output "postgres_networking_data" {
  value = var.postgres_networking_data
}
