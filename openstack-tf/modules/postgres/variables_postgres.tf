
# Variables.Override them to create a proper instance plan

# Postgres instances 
variable "postgres_instance_data" {
  type = map(object({
    name              = string
    description       = string
    flavor            = string
    networks          = list(string)
    security_groups   = list(string)
    server_groups_ids = list(string)
    image_ref         = map(string)
    image_volume_size = optional(number, 10)
    volume            = map(string)
  }))
  default = {
    postgres_service = {
      name              = "",
      description       = "",
      flavor            = "",
      networks          = [],
      security_groups   = [],
      server_groups_ids = [],
      image_ref         = {},
      volume            = {}
    }
  }

}

# Postgres networking configuration.
# NB. use this configuration by settings properly all the key fields 
variable "postgres_networking_data" {
  description = "Data for the PostgreSQL server, including network CIDR and server IP"
  type        = map(string)
  # default = {
  #   networking_security_group_name = "my network security name"
  #   network_cidr   = "192.168.0.0/22"
  #   server_ip      = "192.168.0.5"
  #   network_name = "the_network_name"
  #   server_cidr    = "192.168.0.5/22"
  #   port_range_min = 5432
  #   port_range_max = 5432
  # }

  default = {
    networking_security_group_name = ""
    network_cidr                   = ""
    server_ip                      = ""
    network_name                   = ""
    server_cidr                    = ""
    port_range_min                 = ""
    port_range_max                 = ""
  }
}
