output "keycloak_data" {
  value = var.keycloak_data
}

output "keycloak_recordsets" {
  value = var.keycloak_recordsets
}

output "keycloak_object_store" {
  value = var.keycloak_object_store
}

output "nfs_port_data" {
  value = openstack_compute_interface_attach_v2.nfs_port_to_keycloak
}

output "keycloak_nfs_volume_data" {
  value = openstack_sharedfilesystem_share_v2.keycloak_static
}

output "keycloak_nfs_volume_acls" {
  value = openstack_sharedfilesystem_share_access_v2.keycloak_nfs_share_access
  sensitive = true
}
