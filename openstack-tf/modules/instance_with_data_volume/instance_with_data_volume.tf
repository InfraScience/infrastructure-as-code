
# Module used
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Module used
module "common_variables" {
  source = "../../modules/common_variables"
}

resource "openstack_blockstorage_volume_v3" "instance_data_volume" {
  for_each = var.instances_with_data_volume_map
  name     = each.value.volume.name
  size     = each.value.volume.size
}


# Generic smartgears_service instance
resource "openstack_compute_instance_v2" "instance_with_data_volume" {
  for_each                = var.instances_with_data_volume_map
  name                    = each.value.name
  availability_zone_hints = module.common_variables.availability_zone_no_gpu_name
  flavor_name             = each.value.flavor
  key_pair                = module.ssh_settings.ssh_key_name
  security_groups         = each.value.security_groups
  block_device {
    uuid                  = each.value.image_ref.uuid
    source_type           = "image"
    volume_size           = each.value.image_volume_size
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  # Creates the networks according to input networks 
  dynamic "network" {
    for_each = each.value.networks
    content {
      name = network.value
    }
  }

  # Creates the scheduler_hints (i.e. server groups) according to input server_groups_ids
  dynamic "scheduler_hints" {
    for_each = each.value.server_groups_ids
    content {
      group = scheduler_hints.value
    }
  }

  # user_data script used
  user_data = file("${each.value.image_ref.user_data_file}")
  # Do not replace the instance when the ssh key changes
  lifecycle {
    ignore_changes = [
      # Ignore changes to tags, e.g. because a management agent
      # updates these based on some ruleset managed elsewhere.
      key_pair, user_data, network
    ]
  }
}


# Attach the additional volume
resource "openstack_compute_volume_attach_v2" "attach_volume" {
  for_each    = var.instances_with_data_volume_map
  instance_id = openstack_compute_instance_v2.instance_with_data_volume[each.key].id
  volume_id   = openstack_blockstorage_volume_v3.instance_data_volume[each.key].id
  device      = each.value.volume.device
  depends_on  = [openstack_compute_instance_v2.instance_with_data_volume]
}
