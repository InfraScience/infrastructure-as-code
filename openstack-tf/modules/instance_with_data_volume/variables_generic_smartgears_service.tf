
#Default smartgears_service is EMPTY. Override it to create a proper smartegears plan
variable "instances_with_data_volume_map" {
  type = map(object({
    name              = string
    description       = string
    flavor            = string
    networks          = list(string)
    security_groups   = list(string)
    server_groups_ids = list(string)
    image_ref         = map(string)
    image_volume_size = optional(number, 10)
    volume            = map(string)
  }))
  default = {
    smartgears_service = {
      name              = "",
      description       = "",
      flavor            = "",
      networks          = [],
      security_groups   = [],
      server_groups_ids = [],
      image_ref         = {},
      volume            = {}
    }
  }

}
