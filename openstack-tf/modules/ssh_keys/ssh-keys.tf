# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

resource "openstack_compute_keypair_v2" "initial_ssh_key" {
  name       = module.ssh_settings.ssh_key_name
  public_key = file("${module.ssh_settings.ssh_key_file}.pub")
}
