output "ubuntu_1804" {
  value = var.ubuntu_1804
}

output "ubuntu1804_data_file" {
  value = var.ubuntu1804_data_file
}


output "ubuntu_2204" {
  value = var.ubuntu_2204
}

output "ubuntu2204_data_file" {
  value = var.ubuntu2204_data_file
}

output "ubuntu_2404" {
  value = var.ubuntu_2404
}

output "ubuntu2404_data_file" {
  value = var.ubuntu2404_data_file
}

output "ssh_sources" {
  value = var.ssh_sources
}

