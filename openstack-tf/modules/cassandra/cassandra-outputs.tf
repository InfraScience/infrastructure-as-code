output "cassandra_data" {
  value = var.cassandra_server_data
}

output "cassandra_ip_addrs" {
  value = var.cassandra_ip
}

output "cassandra_network" {
  value = var.cassandra_net
}

output "cassandra_tcp_ports" {
  value = var.cassandra_tcp_ports_map
}
