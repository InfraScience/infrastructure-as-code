variable "cassandra_server_data" {
  type = map(string)
  default = {
    node_name             = "cassandra"
    node_data_disk_size   = 20
    node_data_disk_device = "/dev/vdb"
  }
}

variable "cassandra_affinity_policy" {
  default = "soft-anti-affinity"
}

variable "cassandra_node_flavor" {
  default = ""
}

variable "cassandra_nodes_count" {
  default = 0
}

variable "cassandra_ip" {
  type    = list(string)
  default = ["192.168.13.5", "192.168.13.6", "192.168.13.7"]
}

variable "cassandra_cidr" {
  type    = list(string)
  default = ["192.168.13.5/32", "192.168.13.6/32", "192.168.13.7/32"]
}

variable "cassandra_net" {
  type = map(string)
  default = {
    network_name          = "cassandra-net"
    network_description   = "Network used by the Cassandra cluster and clients to access the service"
    network_cidr          = "192.168.13.0/24"
    allocation_pool_start = "192.168.13.20"
    allocation_pool_end   = "192.168.13.254"
  }
}

variable "cassandra_tcp_ports_map" {
  type = map(object({
    description = string
    port_min    = number
    port_max    = number
  }))
  default = {
    "cassandra_ports" = {
      description = ""
      port_min    = 7000
      port_max    = 7000
    }
  }
}
