#
variable "os_project_data" {
  type = map(string)
  default = {
    id = "a2de533851354b1f8d99ac6b6216d92e"
  }
}

variable "os_region_data" {
  type = map(string)
  default = {
    name = "garr-ct1"
  }
}

variable "default_security_group" {
  type = map(string)
  default = {
    name = "default"
    id = "c7499f6c-147c-4b61-9a29-1a6ca4a85a98"
  }
}

variable "availability_zone" {
  type = map(string)
  default = {
    name = "nova"
  }
}

variable "main_private_network" {
  type = map(string)
  default = {
    name        = "isti-VM-CT1-net"
    description = "GARR CT1 main network"
    id          = "310d65b2-fc5b-4079-b5c2-eb286fbde757"
  }
}

variable "mtu_size" {
  default = 9000
}

variable "main_private_subnet" {
  type = map(string)
  default = {
    name             = "isti-VM-subnet"
    description      = "GARR-CT1 main subnet"
    cidr             = "192.168.100.0/24"
    gateway_ip       = "192.168.100.1"
    allocation_start = "192.168.100.2"
    allocation_end   = "192.168.100.253"
    id               = "780bcac9-3835-436c-901a-339e38e7345f"
  }
}

variable "resolvers_ip" {
  type    = list(string)
  default = ["193.206.141.38", "193.206.141.42"]
}

variable "external_router" {
  type = map(string)
  default = {
    name        = "isti-router"
    description = "GARR-CT1 main router"
  }
}

variable "external_network" {
  type = map(string)
  default = {
    name = "floating-ip"
    id   = "bf588091-e239-4fc8-8d35-eee2bd7d3cb0"
  }
}

variable "jump_proxy_ssh_shell" {
  type = map(string)
  default = {
    hostname   = "shell.garr-ct1.d4science.net"
    private_ip = "192.168.100.4"
    cidr = "192.168.100.4/24"
    public_ip = "90.147.166.130"
  }
}

variable "prometheus_host" {
  type = map(string)
  default = {
    hostname   = "ip-90-147-166-113.ct1.garrservices.it"
    private_ip = "192.168.100.15"
  }
}
