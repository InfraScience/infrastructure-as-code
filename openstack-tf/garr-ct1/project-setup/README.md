# How to obtain the "generated" data

We can import resources using the `terraform import command. The terraform tasks must be already defined for the import to be successful.
Those resources have been imported running:

```shell-session
terraform import openstack_networking_network_v2.main-private-network 310d65b2-fc5b-4079-b5c2-eb286fbde757
terraform import openstack_networking_subnet_v2.main-private-subnet 780bcac9-3835-436c-901a-339e38e7345f
terraform import openstack_networking_router_v2.external-router e621a18a-d8f8-4b5a-a9e7-d1ebc9f82a91
terraform import openstack_networking_router_interface_v2.private-network-routing 2979ba87-2498-4a75-8fcf-b11bb4dadf7a
```

