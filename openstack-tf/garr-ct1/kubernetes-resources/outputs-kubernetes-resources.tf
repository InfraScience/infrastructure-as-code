output "k8s_controllers" {
  value     = openstack_compute_instance_v2.docker_k8s_controllers
  sensitive = true
}

output "k8s_workers" {
  value     = openstack_compute_instance_v2.docker_k8s_workers
  sensitive = true
}

output "k8s_public_ip" {
  value     = openstack_networking_floatingip_v2.k8s_lb_ip.address
}
