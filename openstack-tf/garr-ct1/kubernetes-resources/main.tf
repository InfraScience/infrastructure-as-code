# Define required providers
terraform {
  required_version = ">= 0.16.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.54.0"
    }
  }
}

data "terraform_remote_state" "privnet_dns_router" {
  backend = "local"

  config = {
    path = "../project-setup/terraform.tfstate"
  }
}

# SSH settings
module "ssh_settings" {
  source = "../../modules/ssh-key-ref"
}

# Global variables (constants, really)
module "common_variables" {
  source = "../../modules/garr_common_variables"
}
