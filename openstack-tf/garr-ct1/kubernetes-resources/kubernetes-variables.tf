variable "k8s_controllers_data" {
  type = map(string)
  default = {
    count                   = 3
    name                    = "k8s-controller"
    flavor                  = "m1.large"
    boot_disk_size          = 10
    etcd_disk_size          = 4
    docker_disk_size        = 20
    image_type_name         = "Ubuntu 22.04 - GARR"
    image_type_uuid         = "94618f26-de42-4b1a-80a0-a88b73391a0a"
    availability_zone_hints = "nova"
  }
}

variable "k8s_workers_data" {
  type = map(string)
  default = {
    count                   = 9
    name                    = "k8s-worker"
    flavor                  = "m2.xlarge"
    boot_disk_size          = 10
    docker_disk_size        = 150
    image_type_name         = "Ubuntu 22.04 - GARR"
    image_type_uuid         = "94618f26-de42-4b1a-80a0-a88b73391a0a"
    availability_zone_hints = "nova"
  }
}

variable "octavia_kubernetes_data" {
  type = map(string)
  default = {
    k8s_lb_name        = "d4s-garr-ct1-k8s-lb-l4"
    k8s_lb_description = "L4 balancer that serves the D4Science Kubernetes at GARR-CT1"
    # openstack --os-cloud garr-ct1 server list
    # does not return any flavor
    # octavia_flavor     = ""
    # octavia_flavor_id  = ""
  }
}